<?php
/**
 * TbDataColumn class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

Yii::import('zii.widgets.grid.CDataColumn');

/**
 * Bootstrap grid data column.
 */
class TbDataColumn extends CDataColumn
{
    public $filterWrapTag = null;
    public $filterWrapHtmlOptions = array();
	/**
	 * Renders the header cell content.
	 * This method will render a link that can trigger the sorting if the column is sortable.
	 */
	protected function renderHeaderCellContent()
	{
		if ($this->grid->enableSorting && $this->sortable && $this->name !== null)
		{
			$sort = $this->grid->dataProvider->getSort();
			$label = isset($this->header) ? $this->header : $sort->resolveLabel($this->name);

			if ($sort->resolveAttribute($this->name) !== false)
				$label .= '<span class="caret"></span>';

			echo $sort->link($this->name, $label, array('class'=>'sort-link'));
		}
		else
		{
			if ($this->name !== null && $this->header === null)
			{
				if ($this->grid->dataProvider instanceof CActiveDataProvider)
					echo CHtml::encode($this->grid->dataProvider->model->getAttributeLabel($this->name));
				else
					echo CHtml::encode($this->name);
			}
			else
				parent::renderHeaderCellContent();
		}
	}

	/**
	 * Renders the filter cell.
	 */
	public function renderFilterCell()
	{
        echo CHtml::openTag('td');
        if(isset($this->filterWrapTag)) {
            echo CHtml::openTag($this->filterWrapTag,$this->filterWrapHtmlOptions);
        }
        echo CHtml::openTag('div', CMap::mergeArray(array('class'=>'filter-container'), $this->filterHtmlOptions));
		$this->renderFilterCellContent();
		echo CHtml::closeTag('div');
        if(isset($this->filterWrapTag)) {
            echo CHtml::closeTag($this->filterWrapTag);
        }
        echo CHtml::closeTag('td');
	}
}
