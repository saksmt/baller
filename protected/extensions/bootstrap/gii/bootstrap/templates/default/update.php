<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->name;
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Изменение',
);\n";
?>

$this->pageTitle = Yii::app()->name.' - Изменение <?php echo $this->name; ?>';

$this->menu=array(
	array('label'=>'Список <?php echo $this->name; ?>','url'=>array('index')),
	array('label'=>'Создание <?php echo $this->name; ?>','url'=>array('create')),
	array('label'=>'Просмотр <?php echo $this->name; ?>','url'=>array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Администрирование <?php echo $this->name; ?>','url'=>array('admin')),
);
?>

<h1>Изменение <?php echo $this->name." <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>