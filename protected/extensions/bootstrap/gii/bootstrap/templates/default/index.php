<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->name;
echo "\$this->breadcrumbs=array(
	'$label',
);\n";
?>

$this->pageTitle = Yii::app()->name.' - <?php echo $this->name; ?>';

$this->menu=array(
	array('label'=>'Создание <?php echo $this->name; ?>','url'=>array('create')),
	array('label'=>'Администрирование <?php echo $this->name; ?>','url'=>array('admin')),
);
?>

<h1><?php echo $label; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
