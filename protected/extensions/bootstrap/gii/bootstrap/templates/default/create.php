<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->name;
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Создание',
);\n";
?>

$this->pageTitle = Yii::app()->name.' - Создание <?php echo $this->name; ?>';

$this->menu=array(
	array('label'=>'Просмотр <?php echo $this->name; ?>','url'=>array('index')),
	array('label'=>'Администрирование <?php echo $this->name; ?>','url'=>array('admin')),
);
?>

<h1>Создать <?php echo $this->name; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
