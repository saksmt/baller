<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->name;
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->pageTitle = Yii::app()->name.' - Просмотр <?php echo $this->name; ?>';

$this->menu=array(
	array('label'=>'Список <?php echo $this->name; ?>','url'=>array('index')),
	array('label'=>'Создание <?php echo $this->name; ?>','url'=>array('create')),
	array('label'=>'Изменение <?php echo $this->name; ?>','url'=>array('update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Удаление <?php echo $this->name; ?>','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Вы уверены, что хотите удалить?')),
	array('label'=>'Администрирование <?php echo $this->name; ?>','url'=>array('admin')),
);
?>

<h1>Просмотр <?php echo $this->name." №<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>
