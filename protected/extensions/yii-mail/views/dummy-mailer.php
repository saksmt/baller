<?php
/**
 * @var CController $this
 * @var YiiMailMessage $message
 */

$to = '<li>'.implode('</li><li>', $message->to).'</li>';
$headers = '<li>'.implode('</li><li>', $message->headers->getAll()).'</li>';

?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo '"'.$message->subject.'" - '.date('d.m.y h:i:s');?></title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Mail info:</h1>
        <div>
            <ul> To:
                <?php echo $to; ?>
            </ul>
            <ul> Headers:
                <?php echo $headers; ?>
            </ul>
        </div>
        <h1>Mail body:</h1>
        <iframe src="./<?php echo $bodyLink; ?>"></iframe>
    </body>
</html>