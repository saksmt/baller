<?php

interface IMailer
{
    public function send(YiiMailMessage $message, array &$failedRecipients);
    public function batchSend(YiiMailMessage $message, array &$failedRecipients = null, Swift_Mailer_RecipientIterator $it = null);
    public function sendSimple($from, $to, $subject, $body);
} 