<?php

class CDummyMailer extends CApplicationComponent implements IMailer
{
    public $logging = true;
    public $dryRun = false;
    public $transportType = '';
    public $transportOptions = array();
    public $viewPath = 'application.views.mail';

    private $controller = null;
    private static $registeredScripts = false;

    public function send(YiiMailMessage $message, array &$failedRecipients = null)
    {
        return $this->_dummySend($message);
    }

    public function batchSend(YiiMailMessage $message, array &$failedRecipients = null, Swift_Mailer_RecipientIterator $it = null)
    {
        return $this->_dummySend($message);
    }

    public function sendSimple($from, $to, $subject, $body)
    {
        $message = new YiiMailMessage;
        $message->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body, 'text/html');

        return $this->_dummySend($message);
    }

    public function init()
    {
        $this->registerScripts();
        parent::init();
    }

    private function _dummySend(YiiMailMessage $message)
    {
        $logPath = Yii::getPathOfAlias('application.runtime.Mailer');
        $viewPath = Yii::getPathOfAlias('ext.yii-mail.views.dummy-mailer').'.php';
        @mkdir($logPath);
        $controller = $this->getController();
        $bodyLink = DIRECTORY_SEPARATOR.$message->subject.'-'.time().'-body.html';
        $log = $controller->renderInternal($viewPath, array('message'=>$message, 'bodyLink'=>$bodyLink), true);
        @file_put_contents($logPath.$bodyLink,$message->body);
        $logPath .= DIRECTORY_SEPARATOR.$message->subject.'-'.time().'.html';
        @file_put_contents($logPath,$log);
        return count($message->to);
    }

    public function registerScripts()
    {
        if (self::$registeredScripts) return;
        self::$registeredScripts = true;
        require dirname(__FILE__).'/vendors/swiftMailer/classes/Swift.php';
        Yii::registerAutoloader(array('Swift','autoload'));
        require dirname(__FILE__).'/vendors/swiftMailer/swift_init.php';
    }

    private function getController()
    {
        if(!isset($this->controller)) {
            $controller = null;
            if(isset(Yii::app()->controller)) {
                $controller = Yii::app()->controller;
            } else {
                $controller = new CController('YiiMail');
            }
            $this->controller = $controller;
        }
        return $this->controller;
    }
}