<?php

class TbMultiFileUpload extends CInputWidget
{
    public $max = -1;
    public $addFileIcon = 'plus';
    public $containerClass = 'panel';
    public $deleteFileIcon = 'remove';
    public $addButtonType = 'primary';
    public $removeButtonType = 'danger';
    public $removeButtonTooltip = 'Удалить файл';
    public $addButtonText = 'Загрузить ещё один файл';

    public function run()
    {
        list($name,$id)=$this->resolveNameID();
        if(substr($name,-2)!=='[]')
            $name.='[]';
        if(isset($this->htmlOptions['id']))
            $id=$this->htmlOptions['id'];
        else
            $this->htmlOptions['id']=$id;
        $this->registerJs();
        echo CHtml::openTag('div', array('class'=>'hidden','id'=>$this->htmlOptions['id']));
        $this->htmlOptions['id'] = '';
        echo CHtml::fileField($name,'',$this->htmlOptions);
        echo CHtml::closeTag('div');
    }

    private function registerJs()
    {
        $cs = Yii::app()->clientScript->addPackage('TbMultiFileUpload', array(
            'basePath'=>'local.widgets.assets',
            'js'=>array(
                'js/TbMultiFileUpload.js',
            ),
            'css'=>array(
                'css/TbMultiFileUpload.css',
            ),
            'depends'=>array('jquery'),
        ))->registerPackage('TbMultiFileUpload');
        $options = CJavaScript::encode($this->getOptions());
        $cs->registerScript('TbMultiFileUpload#'.$this->htmlOptions['id'],'TbMultiFileUpload('.$options.')');
    }

    private function getOptions()
    {
        $addButton = $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'button',
            'type'=>$this->addButtonType,
            'icon'=>$this->addFileIcon,
            'label'=>$this->addButtonText,
            'htmlOptions'=>array(
                'id'=>$this->htmlOptions['id'].'-addButton',
            ),
        ),true);
        $removeButton = $this->widget('bootstrap.widgets.TbButtonGroup',array(
            'buttons'=>array(
                array('url'=>'#', 'label'=>'', 'htmlOptions'=>array(
                    'disabled'=>true,
                )),
                array(
                    'url'=>'#',
                    'label'=>'',
                    'icon'=>$this->deleteFileIcon,
                    'type'=>$this->removeButtonType,
                    'htmlOptions'=>array(
                        'rel'=>'tooltip',
                        'title'=>$this->removeButtonTooltip,
                    ),
                )
            )
        ),true);
        return array(
            'max'=>$this->max,
            'container'=>$this->containerClass,
            'addButton'=>$addButton,
            'removeButton'=>$removeButton,
            'id'=>$this->htmlOptions['id'],
        );
    }
}