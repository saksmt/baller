<?php

Yii::import('bootstrap.widgets.TbListView');

class TbSortedListView extends TbListView
{
    public $ascSortIcon;
    public $descSortIcon;
    public $sortButtonType = null;
    public $sortStacked = false;
    public $sorterSize = null;

    public function renderSorter()
    {
        if ($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes)) {
            return;
        }
        $sort=$this->dataProvider->getSort();
        $links = array(array(
            'label'=>($this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader),
            'buttonType'=>'button',
            'htmlOptions'=>array('disabled'=>'disabled'),
        ));
        foreach ($this->sortableAttributes as $name=>$label) {
            $attributeName = is_integer($name)?$label:$name;
            $linkLabel = is_integer($name)?$sort->resolveLabel($attributeName):$label;
            $sortDirection = $sort->getDirection($attributeName);
            $linkOptions = array();
            $icon = null;
            $link = $sort->createUrl(Yii::app()->getController(),array($attributeName=>!$sortDirection));
            $realLink = array();
            if (isset($sortDirection)) {
                if ($sortDirection === CSort::SORT_DESC) {
                    $icon = $this->ascSortIcon;
                } else {
                    $icon = $this->descSortIcon;
                }
                $realLink['active'] = true;
            }
            if (isset($icon)) {
                $realLink['icon'] = $icon;
            }
            $realLink['url'] = $link;
            $realLink['label'] = $linkLabel;
            $realLink['htmlOptions'] = $linkOptions;
            $links[] = $realLink;
        }
        $widgetParams = array(
            'buttons'=>$links,
            'stacked'=>$this->sortStacked,
            'htmlOptions'=> array('class'=>$this->sorterCssClass),
            'size'=>$this->sorterSize,
        );
        if (isset($this->sortButtonType)) {
            $widgetParams['type'] = $this->sortButtonType;
        }
        $this->widget('bootstrap.widgets.TbButtonGroup',$widgetParams);
        echo $this->sorterFooter;
    }
} 