<?php

/**
 * Creates carousel items from model array
 */
class NewsWidget extends CWidget {

    private $_struct;
    private $_exp;
    private $_data;
    private $_params = array(
        'order'=>'`date` DESC',
        'limit'=>5,
        'struct'=>array(
            'label'=>'title',
            'caption'=>'contents',
        ),
        'expression'=>array(
            'label'=>null,
            'caption'=>null,
            'image'=>null,
        ),
        'link'=>true,
    );
    public $params;
    private $_link;

    public function init()
    {
        extract($this->_params);
        extract($this->params);
        $criteria = new CDbCriteria();
        $criteria->order = $order;
        $criteria->limit = $limit;
        $this->_struct = $struct;
        $this->_exp = $expression;
        $this->_link = $link;
        $this->_data = $model->findAll($criteria);
        $this->widget('bootstrap.widgets.TbCarousel', array(
            'items'=>$this->_parse(),
        ));
    }

    public function run()
    {
    }

    private function _parse()
    {
        $items = array();
        foreach($this->_data as $model) {
            $iPath = Yii::app()->createUrl('/images/').'/none.png';
            if(isset($this->_struct['image']) && $model->{$this->_struct['image']}) {
                $iPath = Yii::app()->baseUrl.'/images/'.get_class($model).'/'.$model->{$this->_struct['image']};
            } elseif(is_callable($this->_exp['image'])) {
                $iPath = Yii::app()->baseUrl.'/images/'.get_class($model).'/'.call_user_func($this->_exp['image'],$model);
            }
            $caption = '';
            if(isset($this->_struct['caption'])) {
                $caption = substr($model->{$this->_struct['caption']}, 0, 200) . (strlen($model->{$this->_struct['caption']}) > 200 ? '...' : '');
            }
            $label = $model->{$this->_struct['label']};
            if($this->_link) {
                $label = CHtml::link($label, array('view','id'=>$model->id));
            }
            $items[] = array(
                'caption'=>$label,
                'label'=>$caption,
                'image'=>$iPath
            );
        }
        return $items;
    }
}