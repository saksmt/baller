-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 14 2014 г., 15:27
-- Версия сервера: 5.5.37-log
-- Версия PHP: 5.5.13-pl0-gentoo

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `baller`
--
CREATE DATABASE IF NOT EXISTS `baller` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `baller`;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `name`, `parent_id`) VALUES
(9, 'Прихожие', NULL),
(10, 'Кухни', NULL),
(11, 'Спальни', NULL),
(12, 'Встраиваемая мебель', NULL),
(13, 'Кровати', 11),
(14, 'Табуретки', 11),
(15, 'Тумбы', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_gallery`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `image` varchar(85) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `title`, `image`, `link`) VALUES
(1, 'Gallery Element', '956ace313f14c53eafab12eb68918c7b0cee1af4803fb3e85d49bbe5fed2a75f825abaeaa7475fe4.png', 'http://localhost/baller2'),
(2, '3vsdvs', '3e43c6841b7b8daa9e288e1d5df213b9ce5c5967a31ea769b8e78e5e5ec6cb127bb2bf7fb57da470.png', 'http://localhost/');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_materials`
--

CREATE TABLE IF NOT EXISTS `tbl_materials` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `image` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_materials`
--

INSERT INTO `tbl_materials` (`id`, `name`, `comment`, `code`, `image`) VALUES
(1, 'Ясень', 'он самый!', '234224', 'a37047a1f27ca85b49af61f382f7348c2c5017348b1a6e9dbead32755d72b836e6d38dd4b5033eff.png'),
(2, 'Дуб', 'Деревянный', '35', 'a37047a1f27ca85b49af61f382f7348c2c5017348b1a6e9dbead32755d72b836e6d38dd4b5033eff.png');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `contents` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `contents`, `date`, `image`) VALUES
(1, 'test', 'test text', '2014-05-19 10:41:50', NULL),
(2, 'asdasd', 'asdasdasd', '2014-05-22 14:53:23', NULL),
(3, 'fwefwef', 'sefsfwef', '2014-05-22 14:53:35', NULL),
(4, 'Hello world', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2014-05-22 15:12:52', NULL),
(5, 'ferferf', 'erferfer', '2014-05-24 07:27:11', NULL),
(6, 'ferfe', 'erfefsfgb', '2014-05-24 07:27:23', NULL),
(7, 'u.iy,utmyj', 'bcbcnmuu', '2014-05-24 07:27:37', NULL),
(8, 'mygmhjhmg', 'mhgjmghjm', '2014-05-24 07:27:49', NULL),
(9, 'hjm', 'ghjmgh', '2014-05-24 07:28:12', NULL),
(10, 'yumu', 'myumy', '2014-05-24 07:28:19', NULL),
(11, 'vbxb', 'gbfg', '2014-05-24 07:28:26', NULL),
(12, 'mhjm', 'nhgnghn', '2014-05-24 07:28:38', NULL),
(13, 'oiuyt', 'tmuyu', '2014-05-24 07:28:45', NULL),
(14, 'wert', 'dfgb', '2014-05-24 07:28:52', NULL),
(15, ',,uy,', 'rtegt', '2014-05-24 07:28:59', NULL),
(16, 'ertgrg', 'ertgert', '2014-05-24 07:29:06', NULL),
(17, 'gertgmtyum', 'yhrtyhrtyh', '2014-05-24 07:29:13', NULL),
(19, 'hello hell', 'wwrghryfn', '2014-06-30 09:00:55', '536e8fb34d111ae3af7e4d8cc61732fac5549186c43e17bc686d6670bae5eee0701e86fe0b4ae175.png'),
(20, '123', 'function twoDigits(d) {\r\n    if(0 <= d && d < 10) return "0" + d.toString();\r\n    if(-10 < d && d < 0) return "-0" + (-1*d).toString();\r\n    return d.toString();\r\n};', '2014-01-02 16:00:00', '536e8fb34d111ae3af7e4d8cc61732fac5549186c43e17bc686d6670bae5eee0701e86fe0b4ae175.png');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_orders`
--

CREATE TABLE IF NOT EXISTS `tbl_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `user` int(11) unsigned DEFAULT NULL,
  `products` text NOT NULL,
  `telephone` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cost` int(6) NOT NULL,
  `address` text NOT NULL,
  `status` enum('waiting','processing','declined','ready','grabbed') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `width` int(4) unsigned NOT NULL,
  `height` int(4) unsigned NOT NULL,
  `length` int(4) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `material_id` int(11) unsigned NOT NULL,
  `description` text NOT NULL,
  `status` enum('available','not_available','order_only','') NOT NULL,
  `images` text,
  `price` float unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `title`, `width`, `height`, `length`, `category_id`, `material_id`, `description`, `status`, `images`, `price`) VALUES
(1, 'Ящик', 2000, 2000, 2000, 13, 1, 'Просто ящик', 'order_only', NULL, 0),
(2, 'Палка', 2, 3, 4, 15, 2, 'Хех', 'order_only', 'a:2:{i:0;s:84:"536e8fb34d111ae3af7e4d8cc61732fac5549186c43e17bc686d6670bae5eee0701e86fe0b4ae175.png";i:1;s:84:"4aae1af978bf1510cb4b0636281e8c012725442629a29777418f3be95f557e7b10a7af6a18cab8ed.png";}', 0),
(3, 'Пень', 1221, 1212, 3245, 14, 2, ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'order_only', 'a:2:{i:0;s:84:"536e8fb34d111ae3af7e4d8cc61732fac5549186c43e17bc686d6670bae5eee0701e86fe0b4ae175.png";i:1;s:84:"a37047a1f27ca85b49af61f382f7348c2c5017348b1a6e9dbead32755d72b836e6d38dd4b5033eff.png";}', 0),
(4, 'Товар 0', 22, 22, 22, 9, 1, 'товар', 'available', NULL, 0),
(5, 'товар 1', 222, 2222, 2222, 9, 1, 'товар', 'available', NULL, 0),
(6, 'товар 2', 327, 327, 389, 9, 1, 'товар', 'available', NULL, 0),
(7, 'товар 3', 1312, 21, 234, 9, 1, 'описание', 'available', NULL, 0),
(8, 'товар 4', 2346, 8765, 3435, 9, 1, 'опимсание', 'available', NULL, 0),
(9, 'товар 5', 5893, 357, 4622, 9, 1, 'товар', 'available', NULL, 200),
(10, 'товар 6', 4758, 4626, 3864, 9, 1, 'товар', 'available', NULL, 5234),
(11, 'товар 7', 367, 3738, 3266, 9, 1, 'товар', 'available', NULL, 7827),
(12, 'товар 8', 3262, 8954, 6321, 9, 1, 'товар', 'available', NULL, 32787),
(13, 'товар 9', 4378, 3672, 8427, 9, 1, 'товар', 'available', NULL, 34627),
(14, 'товар 10', 2377, 7327, 5635, 9, 1, 'товар', 'available', NULL, 426766);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `passwd` varchar(64) NOT NULL,
  `role` enum('user','admin','inactive') NOT NULL DEFAULT 'inactive',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `login`, `email`, `name`, `passwd`, `role`) VALUES
(5, 'root', 'root@this.local', 'God', '$2a$13$WNl./1xRLDBz8NEPEYKNaucVpeQfSEcUZwJbguOokvBpahvGIr.0u', 'admin'),
(19, 'saksmt', 'kirillsaksin@yandex.ru', 'Саксин Кирилл Геннадьевич', '$2a$13$46b0g79Oj7zRgtwaDNzM6u.tcP7biaSsd2rpnNXEarWZv18ikuDLG', 'admin'),
(20, '', 'kirill.saksin@rambler.ru', 'Саксин Кирилл Геннадьевич', '$2a$13$JV2ClUqfLFrpo8bLdBWdReS3SlVDuABNmlA0nfoTH23WTEIJHiG6K', 'user'),
(21, 'qwd', 'q@q.q', 'asdf as a', '$2a$13$owWvbT6OY0X3kTrtdvZadeSQQdG27.pOfJX7Ed0Wkib/LMgJortli', 'inactive'),
(22, 'ytu', 'tyu@ycs.we', 'dfhu', '$2a$13$uBHtXHUApgLxRWi7RJ7yauSqka43s3sFuARFtYTv/7CLzQZQR96eC', 'inactive');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
