<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('local', dirname(__FILE__).'/../extensions/local');
Yii::setPathOfAlias('ext', dirname(__FILE__).'/../extensions');

return require(defined('YII_DEBUG') ? 'develop.php' : 'production.php');