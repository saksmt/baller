<?php

return array(

    'categories.view' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'categories.update' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'categories.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'categories.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'categories.create' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'gallery.view' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'gallery.update' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'gallery.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'gallery.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'gallery.create' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'materials.view' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'materials.update' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'materials.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'materials.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'materials.create' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'news.view' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'news.update' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'news.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'news.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'news.create' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'product.view' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'product.update' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'product.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'product.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'product.create' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'panel.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),

    'orders.custom' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'orders.admin' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => null,
        'data' => null,
    ),
    'orders.index' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'alias for action',
        'bizRule' => 'return isset($params["user_id"])||$params["userId"]===$params["user_id"]||$this->checkAccess("admin",$params["userId"])',
        'data' => null,
    ),
    'orders.view' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'alias for action',
        'bizRule' => '$order = Order::model()->findByPk($params["id"]);return !isset($order)||$order->user_id==$params["userId"]||$this->checkAccess("admin",$params["userId"]);',
        'data' => null,
    ),

    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'data' => null,
        'children' => array(
            'gallery.view',
            'gallery.index',
            'news.view',
            'news.index',
            'product.index',
            'product.view',
        ),
    ),
    'inactive' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Не подтверждён',
        'bizRule' => null,
        'data' => null,
        'children' => array(
            'guest',
            'orders.view',
            'orders.index',
        ),
    ),
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Пользователь',
        'children' => array(
            'inactive',
            'orders.custom',
        ),
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Администратор',
        'children' => array(
            'user',
            'categories.view',
            'categories.index',
            'categories.admin',
            'categories.update',
            'categories.create',
            'materials.view',
            'materials.index',
            'materials.admin',
            'materials.update',
            'materials.create',
            'gallery.update',
            'gallery.admin',
            'gallery.create',
            'news.update',
            'news.admin',
            'news.create',
            'product.update',
            'product.admin',
            'product.create',
            'panel.index',
            'orders.admin',
        ),
        'bizRule' => null,
        'data' => null
    ),
);
