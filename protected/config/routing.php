<?php
return array(
    'orders/list/<user_id:\w+>' => 'orders/index',
    ''=>'site/index',
    'page/<view:\w+>'=>'site/page',
    'error'=>'site/error',
    'user/activate/<id:\d+>/<securecode:[a-zA-Z0-9\$\.]+>' => 'user/activate',
    'shop/products/<view:table|grid>'=>'shop/products',
    'categories/list/<id:\d+>' => 'categories/index',
    'user/resetPassword/<id:\d+>/<secureCode:[^\s]+>' => 'user/resetPassword',
    '<controller:\w+>' => '<controller>/index',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);