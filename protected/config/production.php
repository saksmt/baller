<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('local', dirname(__FILE__).'/../extensions/local');
Yii::setPathOfAlias('ext', dirname(__FILE__).'/../extensions');

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Баллер',

    // preloading 'log' component
    'preload'=>array('log'),

    'theme'=>'bootstrap',

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'ext.yii-mail.*',
    ),

    'modules'=>array(
    ),

    // application components
    'components'=>array(
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('guest'),
        ),
        'user'=>array(
            'class'=>'WebUser',
            'allowAutoLogin'=>true,
        ),
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>require('routing.php'),
        ),
        // uncomment the following to use a MySQL database
        'db'=>array(
            'connectionString' => 'mysql:host=mysql59.1gb.ru;dbname=gb_baller',
            'emulatePrepare' => true,
            'username' => 'gb_baller',
            'password' => 'f55b81dddvn',
            'charset' => 'utf8',
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'mail'=>array(
            'class'=>'ext.yii-mail.YiiMail',
            'transport' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp-9.1gb.ru',
                'username' => 'u398982',
                'password' => '1e43041f0',
                'port' => 25
            ),
        ),
    ),

    'language'=>'ru',

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'webmaster@oooballer.ru',
    ),
);