<?php


class ProfileController extends Controller
{
    public function getMenu()
    {
        $menu = array(
            array('label' => 'Просмотр', 'url'=>array('profile/index')),
            array('label' => 'Редактирование', 'url' => array('profile/edit')),
        );
        return $menu;
    }

    public function actionIndex()
    {
        $this->render('index', array(
            'user' => Yii::app()->user,
        ));
    }

    public function actionEdit()
    {
        $model = UserEditForm::model()->findByPk(Yii::app()->user->id);
        $this->performAjaxValidation($model);
        if (Yii::app()->request->getParam('UserEditForm') !== null) {
            $model->attributes = Yii::app()->request->getParam('UserEditForm');
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', 'Ваши данные обновлены!');
                } else {
                    Yii::app()->user->setFlash('warning', 'Во время сохранения произошла ошибка!');
                }
            }
        }
        $model->passwd = '';
        $model->passwdRepeat = '';
        $model->password = '';
        $this->render('edit', array('model' => $model));

    }

} 