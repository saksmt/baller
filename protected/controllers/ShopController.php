<?php

class ShopController extends Controller
{
    private $menuItem = array();

    public function setMenuItem($menuItem)
    {
        $this->menuItem = $menuItem;
    }

    public function getMenu()
    {
        $menu = array(
            array('label'=>'Магазин', 'url'=>array('shop/index')),
            array('label'=>'Список категорий', 'url'=>array('shop/category')),
            array('label'=>'Поиск', 'url'=>array('shop/products')),
            array('label'=>'Доставка', 'url'=>array('shop/delivery')),
        );
        if(!empty($this->menuItem)) {
            $menu[] = '---';
            $menu[] = $this->menuItem;
        }
        return $menu;
    }

    public function getRightMenu()
    {
        $cartCount = Cart::instance()->count();
        $menu = array();
        if (!$cartCount) {
            $menu[] = array('label'=>'Корзина '.$this->widget('bootstrap.widgets.TbBadge',array(
                    'type'=>'info',
                    'label'=>$cartCount,
                    'htmlOptions'=>array('id'=>'cart-link-badge'),
                ),true),
                'url'=>array('shop/cart'),
            );
        } else {
            $menu[] = array('label'=>'Корзина '.$this->widget('bootstrap.widgets.TbBadge',array(
                    'type'=>'info',
                    'label'=>$cartCount,
                    'htmlOptions'=>array('id'=>'cart-link-badge'),
                ),true), 'items'=>array(
                array('label'=>'Просмотр','url'=>array('shop/cart')),
                array('label'=>'Перейти к оформлению','url'=>array('/orders/create')),
            ));
        }
        if (!Yii::app()->user->isGuest) {
            $menu[] = array('label'=>'Мои заказы', 'url'=>array('/orders'));
        }
        $menu[] = '---';
        return $menu;
    }

    public function filters()
    {
        return array(
            'ajaxOnly + clearCart, addToCart',
            'postOnly + removeFromCart',
        );
    }

    public function actionIndex()
    {
        $categories = Categories::model()->findAll('`parent_id` IS NULL');
        $this->render('index', array(
            'categories'=>$categories,
        ));
    }

    public function actionCategory($id=null)
    {
        $model = null;
        if(isset($id)) {
            $model = Categories::model()->findByPk($id);
            if (!isset($model)) {
                throw new CHttpException(404, 'Категория не найдена!');
            }
            if (empty($model->children)) {
                $this->redirect($this->createUrl('productsByCategory', array('id' => $id)));
            }
        } else {
            $model = Categories::model()->findAll('`parent_id` IS NULL');
        }
        $this->render('categories', array('model'=>$model));
    }

    public function actionProductsByCategory($id)
    {
        if(!isset($_POST['Product'])) {
            $_POST['Product'] = array();
        }
        $_POST['Product']['category_id'] = $id;
        $this->forward('products');
        Yii::app()->end();
    }

    public function actionProducts($view = 'list')
    {
        $model = new Product('search');
        $model->unsetAttributes();
        if(Yii::app()->request->getParam('Product') !== null) {
            $model->attributes = Yii::app()->request->getParam('Product');
        }
        $dataProvider =$model->search();
        $dataProvider->sort->attributes = array(
            'status'=>array(
                'asc'=>Product::getStatusSort().'ASC',
                'desc'=>Product::getStatusSort().'DESC',
                'label'=>'По наличию',
            ),
            'price'=>array(
                'asc'=>'price ASC',
                'desc'=>'price DESC',
                'label'=>'По цене',
            ),
            'title'=>array(
                'asc'=>'title ASC',
                'desc'=>'title DESC',
                'label'=>'По названию'
            ),
        );
        $dataProvider->pagination->pageSize = 12;
        $dataProvider->sort->defaultOrder = array('title'=>CSort::SORT_ASC);
        $this->render('products', array('dataProvider'=>$dataProvider,'type'=>$view,'model'=>$model));
    }

    public function actionProduct($id)
    {
        $model = Product::model()->findByPk($id);
        if (!isset($model)) {
            throw new CHttpException(404, 'Товар не найден!');
        }
        if(Yii::app()->user->checkAccess('product.update')) {
            $this->menuItem = array('label'=>'Изменение товара', 'url'=>array('/product/update','id'=>$id));
        }
        $this->render('product',array('model'=>$model));
    }

    public function actionDelivery()
    {
        $this->render('delivery');
    }

    public function actionAddToCart($id)
    {
        $model = Product::model()->findByPk($id);
        if (!isset($model)) {
            Yii::app()->user->setFlash('danger','<strong>Ошибка </strong>Товар не найден!');
        } else {
            if($model->isAvailable()) {
                Cart::instance()->add($model);
                Yii::app()->user->setFlash('success', 'Товар добавлен в корзину!');
            } else {
                Yii::app()->user->setFlash('error', 'Вы не можете добавить этот товар в корзину!');
            }
        }
        $this->renderPartial('to_cart',array('cartCount'=>Cart::instance()->count()));
    }

    public function actionClearCart()
    {
        Cart::instance()->clear();
        $this->renderPartial('clear_cart');
    }

    public function actionCart()
    {
        $cart = Cart::instance()->getCart();
        $this->render('cart',array('cart'=>$cart));
    }

    public function actionRemoveFromCart()
    {
        header('Content-Type: application/json; charset=utf-8');
        $id = Yii::app()->request->getParam('id');
        if(!isset($id)) {
            echo CJavaScript::encode(false);
        } else {
            echo CJavaScript::encode(Cart::instance()->remove($id));
        }
        Yii::app()->end();
    }
}