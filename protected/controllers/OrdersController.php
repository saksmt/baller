<?php

/**
 * Class OrdersController
 */
class OrdersController extends Controller
{
    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('create'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('index'),
                'roles'=>array('admin','orders.index'=>$this->actionParams),
            ),
            array('allow',
                'actions'=>array('custom'),
                'roles'=>array('admin','user'),
            ),
            array('allow',
                'actions'=>array('admin','changeStatus'),
                'roles'=>array('admin'),
            ),
            array('allow',
                'actions'=>array('view'),
                'roles'=>array('orders.view'=>$this->actionParams),
            ),
            array('deny','users'=>array('*')),
        );
    }

    public function getMenu()
    {
        $menu = array();
        if (!Yii::app()->user->isGuest) {
            $menu[] = array('label'=>'Мои заказы', 'url'=>array('/orders/index'));
        }
        /// TODO: !!! create have no params
        $actions = array('create','custom');
        if (in_array($this->action->id,$actions)) {
            $item = array('url'=>array($this->route));
            if ($this->action->id === 'create') {
                $item['label'] = 'Оформление заказа';
            } else {
                $item['label'] = '"На Заказ!"';
            }
            $menu[] = $item;
        }
        if (Yii::app()->user->checkAccess('orders.admin')) {
            $menu[] = '---';
            $menu[] = array('label'=>'Заказы пользователей', 'url'=>array('/orders/admin'));
        }
        return $menu;
    }

    public function actionCreate()
    {
        if (!Cart::instance()->count()) {
            Yii::app()->user->setFlash('info', '<strong>Ваша корзина пуста! </strong>Вы не можете купить воздух в этом магазине! Лучше выберите что-нибудь отсюда!');
            $this->redirect(array('/shop/products'));
        }
        $products = Cart::instance()->all();
        $model = new Order();
        $orderParams = Yii::app()->request->getParam('Order');
        if (isset($orderParams)) {
            $model->attributes = $orderParams;
            if ($model->validate()) {
                /// TODO: !REFACTOR! write MANY_MANY trait(dynamic function in __call)
                $tr = Yii::app()->db->beginTransaction();
                $model->products = $products;
                $error = !$model->save();
                $model->refresh();
                //echo $model->id;
                $error |= !OrderProduct::assignProductsToOrder($model, $products);
                if ($error) {
                    $tr->rollback();
                    Yii::app()->user->setFlash('danger', '<strong>Ошибка</strong> Повторите запрос позже!');
                } else {
                    $tr->commit();
                    if (!Yii::app()->user->isGuest) {
                        $mail = new YiiMailMessage();
                        $mail->view = 'order';
                        $mail->setBody(array('order'=>$model), 'text/html', 'utf-8');
                        $mail->addTo(Yii::app()->user->email);
                        $mail->message->setFrom('noreply@'.Yii::app()->request->serverName, 'Баллер Мебель');
                        $mail->subject = 'Заказ №'.$model->id;
                        Yii::app()->mail->send($mail);
                        Yii::app()->user->setFlash('success', 'Заказ успешно оформлен, всю информацию вы можете посмотреть в разделе "Мои заказы" и на электронной почте("'.Yii::app()->user->email.'")');
                    } else {
                        Yii::app()->user->setFlash('success', 'Заказ успешно оформлен, ваш номер заказа - '.$model->id);
                    }
                    Cart::instance()->clear();
                    $this->redirect(array('/shop'));
                    Yii::app()->end();
                }
            }
        }
        $this->render('create', array('model'=>$model));
    }

    public function actionCustom($id)
    {
        $model = new Product();
        $baseData = Product::model()->findByPk($id);
        if (!isset($baseData)) {
            throw new CHttpException(404,'Базовый товар не найден!');
        }
        if (!$baseData->isAvailable()) {
            throw new CHttpException(403,'Этот товар не доступен для заказа!');
        }
        $model->attributes = $baseData->attributes;
        $model->price = 0;
        $model->custom = true;
        $model->prototype_id = $id;
        $model->title .= '("На заказ!")';
        if (Yii::app()->request->getParam('Product') !== null) {
            $model->attributes = Yii::app()->request->getParam('Product');
            if ($model->validate()) {
                if ($model->save()) {
                    Cart::instance()->add($model);
                    Yii::app()->user->setFlash('success','Товар добавлен в вашу корзину! '.CHtml::link('Перейти к оформлению заказа',array('orders/create')));
                    $this->redirect(Yii::app()->user->returnUrl);
                    Yii::app()->end();
                } else {
                    Yii::app()->user->setFlash('error','<strong>Ошибка </strong>Повторите запрос позже!');
                }
            }
        }
        $this->render('custom',array('model'=>$model));
    }

    public function actionIndex($user_id = null)
    {
        $user = null;
        if (isset($user_id)) {
            $user = UserDataModel::model()->findByPk($user_id);
        } else {
            $user = Yii::app()->user;
        }
        if (!isset($user)) {
            throw new CHttpException(404,'Пользователь не найден!');
        }
        $model = new Order('search');
        $model->unsetAttributes();
        if (Yii::app()->request->getParam('Order') !== null) {
            $model->attributes = Yii::app()->request->getParam('Order');
        }
        $model->user_id = $user->id;
        $this->render('index',array('model'=>$model,'user'=>$user));
    }

    public function actionView($id)
    {
        $order = Order::model()->findByPk($id);
        if (!isset($order)) {
            throw new CHttpException(404,'Запрашиваемый заказ не найден!');
        }
        $this->render('view',array('order'=>$order));
    }

    public function actionChangeStatus($id)
    {
        $order = Order::model()->findByPk($id);
        $response = array();
        $response['oldClass'] = 'label-'.Order::getStatusClass($order->status);
        $order->status = Yii::app()->request->getParam('newStatus');
        if ($order->save(false)) {
            $response['status'] = 200;
            $response['newClass'] = 'label-'.Order::getStatusClass($order->status);
            $response['newStatus'] = Order::getStatuses()[$order->status];
        } else {
            Yii::app()->user->setFlash('danger','<strong>Ошибка!</strong> Попробуйте перезагрузить страницу и повторить запрос.');
            $response['message'] = $this->widget('bootstrap.widgets.TbAlert',array(
                'block'=>true,
                'fade'=>true,
            ),true);
        }
        header('Content-Type: text/json; charset="UTF-8"');
        echo json_encode($response);
        Yii::app()->end();
    }

    public function actionAdmin()
    {
        $model = new Order('search');
        $model->unsetAttributes();
        if (Yii::app()->request->getParam('Order') !== null) {
            $model->attributes = Yii::app()->request->getParam('Order');
        }
        $this->render('admin',array('model'=>$model));
    }

}