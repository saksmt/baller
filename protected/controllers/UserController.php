<?php

class UserController extends Controller {

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xF5F5F5,
                'height'=>35,
                'width'=>100,
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('captcha', 'activate'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('index', 'profile', 'logout'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('register','login','forgotPassword', 'resetPassword'),
                'roles'=>array('guest'),
            ),
            array('allow',
                'actions'=>array('admin'),
                'roles'=>array('admin'),
            ),
            array('allow',
                'actions'=>array('update'),
                'roles'=>array('admin'),
                'verbs'=>array('POST'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback'=>array($this, 'redirectNotAuthorized'),
            ),
        );
    }

    public function actionRegister()
    {
        $model = new RegisterForm('insert');
        if(Yii::app()->request->getParam('RegisterForm') !== null) {
            $this->performAjaxValidation($model);
            $model->attributes = Yii::app()->request->getParam('RegisterForm');
            if ($model->validate()) {
                if ($model->save(false)) {
                    Yii::app()->user->setFlash('success', '<strong>Поздравляем!</strong> Вы успешно зарегестрировались. Ожидайте активационное письмо на ' . $model->email);
                    $mail = new YiiMailMessage();
                    $mail->view = 'registered';
                    $mail->setBody(array('user'=>$model), 'text/html', 'utf-8');
                    $mail->addTo($model->email);
                    $mail->message->setFrom('noreply@'.Yii::app()->request->serverName, 'Баллер Мебель');
                    $mail->subject = 'Активация учётной записи';
                    Yii::app()->mail->send($mail);
                    $this->redirect(Yii::app()->user->returnUrl);
                    Yii::app()->end();
                }
                Yii::app()->user->setFlash('danger', '<strong>Упс!</strong> Что-то пошло не так... Повторите попытку позже.');
            }
        }
        $this->render('register', array('model'=>$model));
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        $this->performAjaxValidation($model);
        if(Yii::app()->request->getParam('LoginForm') !== null) {
            $model->attributes = Yii::app()->request->getParam('LoginForm');
            if ($model->validate()) {
                if ($model->login()) {
                    Yii::app()->user->setFlash('success', '<strong>Здравствуйте</strong> Рады видеть вас снова, ' . Yii::app()->user->name . '!');
                    $this->redirect(Yii::app()->user->returnUrl);
                    Yii::app()->end();
                }
            }
        }
        $this->render('login', array('model'=>$model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout(false);
        Yii::app()->user->setFlash('success', '<strong>До свидания</strong> Заходите к нам ещё!');
        $this->redirect(Yii::app()->user->returnUrl);
        Yii::app()->end();
    }

    public function actionActivate($id, $securecode)
    {
        $user = RegisterForm::model()->findByPk($id);
        if (!isset($user) || !$user->verifyCode($securecode)) {
            Yii::app()->user->setFlash('danger', '<strong>Ошибка</strong> Проверьте правильность ссылки!');
        } else {
            $user->role = 'user';
            if($user->save(false)) {
                Yii::app()->user->setFlash('success', '<strong>Поздравляем</strong> Вы успешно активировали аккаунт!');
            } else {
                Yii::app()->user->setFlash('warning', '<strong>Ошибка</strong> Во время активации произошла ошибка. Повторите запрос позже.');
            }
        }
        $this->redirect(array('/'));
        Yii::app()->end();
    }

    public function actionForgotPassword()
    {
        $email = Yii::app()->request->getParam('email');
        if (isset($email) && $email !== '') {
            $user = RegisterForm::model()->findByAttributes(array(
                'email' => $email,
            ));
            if (!isset($user)) {
                Yii::app()->user->setFlash('warning', 'Пользователь с указанным email не найден!');
            } else {
                $mail = new YiiMailMessage();
                $mail->view = 'password';
                $mail->setBody(array('user'=>$user), 'text/html', 'utf-8');
                $mail->addTo($user->email);
                $mail->message->setFrom('noreply@'.Yii::app()->request->serverName, 'Баллер Мебель');
                $mail->subject = 'Восстановление пароля';
                Yii::app()->mail->send($mail);
                Yii::app()->user->setFlash('success', 'Инструкции по восстановлению отправлены на '.$user->email);
                $this->redirect(Yii::app()->user->returnUrl);
                Yii::app()->end();
            }
        }
        $this->render('password');
    }

    public function actionResetPassword($id, $secureCode)
    {
        $user = RegisterForm::model()->findByPk($id);
        if (!isset($user) || !$user->verifyCode($secureCode)) {
            Yii::app()->user->setFlash('danger', '<strong>Ошибка</strong> Проверьте правильность ссылки!');
        } else {
            $password = substr(uniqid(), 0, 8);
            $user->passwd = CPasswordHelper::hashPassword($password);
            if($user->save(false)) {
                $mail = new YiiMailMessage();
                $mail->view = 'password_new';
                $mail->setBody(array(
                    'user' => $user,
                    'password' => $password,
                ), 'text/html', 'utf-8');
                $mail->addTo($user->email);
                $mail->message->setFrom('noreply@'.Yii::app()->request->serverName, 'Баллер Мебель');
                $mail->subject = 'Восстановление пароля';
                Yii::app()->mail->send($mail);
                Yii::app()->user->setFlash('success', '<strong>Поздравляем</strong> Ваш новый пароль отправлен на '.$user->email);
            } else {
                Yii::app()->user->setFlash('warning', '<strong>Ошибка</strong> Во время восстановления пароля произошла ошибка. Повторите запрос позже.');
            }
        }
        $this->redirect(Yii::app()->user->returnUrl);
        Yii::app()->end();
    }

    public function actionAdmin()
    {
        $model = new UserDataModel('search');
        $model->unsetAttributes();
        if(Yii::app()->request->getParam('UserDataModel') !== null) {
            $model->attributes = Yii::app()->request->getParam('UserDataModel');
        }
        $this->render('admin', array('model'=>$model));
    }

    public function actionUpdate($id)
    {
        header('Content-Type: application/json; charset=utf-8');
        $model = UserDataModel::model()->findByPk($id);
        if (!isset($model)) {
            echo CJavaScript::jsonEncode(array('code'=>404,'id'=>$id));
            Yii::app()->end();
        }
        $model->attributes = Yii::app()->request->getParam('UserDataModel');
        $code = $model->save(false)?200:500;
        echo CJavaScript::jsonEncode(array('code'=>$code,'id'=>$id,'attributes'=>$model->attributes,'post'=>Yii::app()->request->getParam('UserDataModel')));
        Yii::app()->end();
    }

}