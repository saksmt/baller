<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.fontello').'.css'));
$this->pageTitle = Yii::app()->name.' - Магазин - Корзина';
$price = 0;
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/cart.js');
Yii::app()->clientScript->registerScript('cart_defines',
    'window.address = \''.$this->createUrl('/shop/removeFromCart'.'\';'.
    'window.errorMessage = "<div class=\'alert in fade alert-block alert-danger\'><a class=\'close\' data-dismiss=\'alert\'>&times;</a><strong>Ошибка</strong> Повторите запрос позже</div>";'
    ));
$orderPermission = Yii::app()->user->checkAccess('orders.custom');
foreach ($cart as $product) {
    $price += $product->price;
}
if (count($cart) > 0):
?>
    <div id="cart">
        <div class="panel search-panel">
            <div class="cart-total span6">
                    <strong>Всего товаров</strong>: <span class="badge badge-info"><?= count($cart);?></span> <strong>На сумму</strong>: <?= $price; ?><i class="icon-rub"></i>
            </div>
            <div class="cart-operations span6 text-right">
                <?php if (count($cart) > 0) {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'type' => 'success',
                        'icon' => 'ok',
                        'url' => array('orders/create'),
                        'label' => 'Оформить заказ',
                    ));
                    echo ' ';
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'type' => 'danger',
                        'buttonType' => 'ajaxLink',
                        'icon' => 'trash',
                        'url' => array('shop/clearCart'),
                        'label' => 'Очистить корзину',
                        'ajaxOptions' => array('update' => '#cart'),
                        'htmlOptions' => array('id'=>'clearCart'),
                    ));
                } else {
                }
                ?>
            </div>
        </div>
        <div id="cart-elements">
            <table class="table table-hover table-striped">
                <tr>
                    <th>Название</th>
                    <th>Цена,<i class="icon-rub"></i></th>
                    <th>Действия</th>
                </tr>
                <?php foreach ($cart as $key => $product):?>
                <tr>
                    <td><?= $product->title; ?></td>
                    <td><?= $product->price; ?></td>
                    <td>
                        <a href="#" rel="tooltip" title="Удалить" class="delete-cart-element" data-key="<?= $key; ?>"><i class="icon icon-trash"></i></a>
                        <?= CHtml::link('<i class="icon icon-eye-open"></i>', array('shop/product','id'=>$product->id),array(
                            'rel'=>'tooltip',
                            'title'=>'Просмотр',
                        )); ?>
                        <?php if ($orderPermission) {
                            echo CHtml::link('<i class="icon icon-pencil"></i>',array('/orders/custom','id'=>$product->id),array(
                                'rel'=>'tooltip',
                                'title'=>'"На заказ!"'
                            ));
                        } ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
<?php else: ?>
<div class="text-center panel">
    <?php
    $randProduct = Product::random();
    echo 'Ваша корзина пуста! Как на счёт "'.CHtml::link($randProduct->title,array('/shop/product','id'=>$randProduct->id)).'"?';
    ?>
</div>
<?php endif; ?>