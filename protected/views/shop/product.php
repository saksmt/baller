<?php

Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.fontello').'.css'));

$this->breadcrumbs=array();
$back = $model->category;
$this->breadcrumbs[] = $model->title;
while(true) {
    if (!isset($back) || is_array($model)) {
        $this->breadcrumbs['Магазин'] = array('index');
        break;
    }
    $this->breadcrumbs[$back->name] = array('shop/category', 'id' => $back->id);
    $back = $back->parent;
}
$this->breadcrumbs = array_reverse($this->breadcrumbs);

$images = array('../none.png');
$this->pageTitle = Yii::app()->name.' - Магазин - '.$model->title;
if(isset($model->images)) {
    $images = unserialize($model->images);
}
?>

<?php
Yii::app()->clientScript->registerScript('image','$(function(){$(".material-image").parent().height($(".material-image").width()+40)});');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/image-viewer.js');
Yii::app()->clientScript->registerScript('image-viewer','var iv = new ImageViewer("#image-viewer","#images");')
?>

<div class="row-fluid">
    <h1 class="span6"><?= $model->title; ?> <span class="label label-<?= $model->statusAsLabelClass; ?>"><?= $model->statuses[$model->status]; ?></span></h1>
    <p class="span6 text-right lead" style="margin-top:14px;margin-bottom:0px;">
        <strong>Цена:</strong> <?= $model->price; ?>руб.
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'success',
            'buttonType'=>'ajaxLink',
            'icon'=>'shopping-cart',
            'url'=>array('shop/addToCart','id'=>$model->id),
            'label'=>'В корзину',
            'ajaxOptions'=>array('update'=>'#alerts'),
        ));
        echo ' ';
        if(!$model->custom) {
            $this->widget('bootstrap.widgets.TbButton', array(
                'type' => 'primary',
                'icon' => 'pencil',
                'url' => array('orders/custom', 'id' => $model->id),
                'label' => '"На заказ!"',
                'disabled' => !Yii::app()->user->checkAccess('orders.custom'),
                'htmlOptions' => array(
                    'rel' => 'tooltip',
                    'title' => 'Не ваш размер? Не устраивает материал? Сделайте на заказ!' . (Yii::app()->user->checkAccess('orders.custom') ? '' : ' Доступно только для зарегестрированных пользователей'),
                ),
            ));
        }
        ?>
    </p>
</div>

<div class="row-fluid">
    <div class="span3" id="images">
        <a href="#" class="thumbnail image-main">
            <div class="product-image" style="background-image: url('<?= Yii::app()->baseUrl.'/images/Product/'.$images[0];?>');"></div>
        </a>
        <div class="panel panel-mini" style="width:100%;max-width:100%;overflow: auto;">
            <?php
            echo '<a class="thumbnail active image-mini" href="#">';
            echo '<div class="product-image" style="background-image:url(\''.Yii::app()->baseUrl.'/images/Product/'.array_shift($images).'\')"></div>';
            echo '</a>';
            foreach ($images as $image) {
                $imagePath = Yii::app()->baseUrl.'/images/Product/'.$image;
                echo '<a class="thumbnail image-mini" href="#">';
                echo '<div class="product-image" style="background-image:url(\''.$imagePath.'\')"></div>';
                echo '</a>';
            }
            ?>
        </div>
    </div>
    <div class="panel span9">
        <?php if(!$model->custom || $model->width != 0):?>
            <p><strong>Ширина: </strong><?= $model->width; ?></p>
            <p><strong>Высота: </strong><?= $model->height; ?></p>
            <p><strong>Глубина: </strong><?= $model->length; ?></p>
        <?php endif; ?>
        <p><strong>Материал: </strong><?= CHtml::link($model->material->name,'#',array(
                'data-toggle'=>'modal',
                'data-target'=>'#materialDescription',
                'id'=>'show-material-button',
            )); ?></p>
        <p><?= $model->description; ?></p>
    </div>
</div>
<?php $this->beginWidget('bootstrap.widgets.TbModal',array(
    'id'=>'materialDescription',
));?>
    <div class="modal-header"><?= $model->material->name; ?></div>
    <div class="modal-body">
        <div class="thumbnail span4">
            <div class="material-image" style="<?= Yii::app()->baseUrl.'/images/Materials/'.(isset($model->material->image)?$model->material->image:'../none.png'); ?>"></div>
        </div>
        <div class="span8">
            <?= $model->material->comment; ?>
            <p>Код материала: <?= $model->material->code; ?></p>
        </div>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Закрыть',
            'url'=>'#',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget();?>
<?php $this->beginWidget('bootstrap.widgets.TbModal',array(
    'id'=>'image-viewer',
));?>
    <div class="modal-body">
        <a class="thumbnail" href="#" id="viewer-main">
            <div class="product-image"></div>
        </a>
        <div id="viewer-menu"></div>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Закрыть',
            'url'=>'#',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget();?>