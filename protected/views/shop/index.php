<?php
$this->pageTitle = Yii::app()->name.' - Магазин';
$this->breadcrumbs = array('Магазин');
$this->widget('local.widgets.NewsWidget', array('params'=>array(
    'order'=>'RAND()',
    'model'=>new Product(),
    'expression'=>array(
        'image'=>function($model) {
            if($model->images) {
                $images = unserialize($model['images']);
                shuffle($images);
                return $images[0];
            }
            return '../none.png';
        },
    ),
    'struct'=>array(
        'caption'=>'description',
        'label'=>'title',
    ),
    'link'=>false,
)));
?>
<div class="panel">
    <h2>Категории:</h2>
    <ul class="unstyled">
        <?php $this->renderPartial('_categories',array('categories'=>$categories,'class'=>'h4'));?>
    </ul>
</div>