<?php
/**
 * @var ShopController $this
 */
?>
<?php
$image = Yii::app()->baseUrl.'/images/none.png';
if(isset($data->images)) {
    $image = Yii::app()->baseUrl.'/images/Product/'.unserialize($data->images)[0];
}
$description = $data->description;
if(strlen($description) > 200) {
    $description = substr($description,0,200).'...';
}
?>
<div class="panel product-in-list">
    <div class="row-fluid">
        <div class="span4">
            <a class="thumbnail" href="<?= $this->createUrl('shop/product', array('id'=>$data->id)); ?>">
                <div class="product-image" style="height:200px;background-image: url('<?= $image; ?>');"></div>
            </a>
        </div>
        <div class="span8 pull-right">
            <div class="span8">
                <h3><?= CHtml::link($data->title,array('shop/product',array('id'=>$data->id))); ?></h3>
                <div class="row-fluid">
                    <div class="span8"><p><?= $description ?></p></div>
                    <div class="span4">
                        <p><strong>Высота: </strong><span class="pull-right"><?= $data->width;?>мм.</span></p>
                        <p><strong>Ширина: </strong><span class="pull-right"><?= $data->height;?>мм.</span></p>
                        <p><strong>Глубина: </strong><span class="pull-right"><?= $data->length;?>мм.</span></p>
                        <p><strong>Артикул: </strong><span class="pull-right"><?php printf('%05d',$data->id); ?></span></p>
                    </div>
                </div>
            </div>
            <div class="span4 text-center pull-right">
                <p class="h3 product-price"><span class="h2"><?= $data->price; ?></span><i class="icon-rub"></i></p>
                <p class="product-status"><span class="label label-<?= $data->statusAsLabelClass; ?>"><?= $data->userStatus; ?></span></p>
                <div><?php $this->widget('bootstrap.widgets.TbButton', array(
                        'type'=>'success',
                        'buttonType'=>'ajaxButton',
                        'size'=>'large',
                        'disabled'=>!$data->isAvailable(),
                        'icon'=>'shopping-cart',
                        'url'=>array('shop/addToCart','id'=>$data->id),
                        'label'=>'В корзину',
                        'ajaxOptions'=>array('update'=>'#alerts'),
                        'htmlOptions'=>$data->isAvailable()?null:array(
                            'rel'=>'tooltip',
                            'title'=>'Эта опция доступна только для зарегестрированных пользователей',
                        ),
                ));?></div>
            </div>
        </div>
    </div>
</div>