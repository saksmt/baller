<?php
/**
 * @var ShopController $this
 * @var string $type
 * @var Product $model
 * @var CActiveDataProvider $dataProvider
 */

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/product-search.js');

if(Yii::app()->user->isGuest) {
    Yii::app()->user->setFlash('info','После '.CHtml::link('регистрации',array('/user/register')).' вы сможете покупать товары со статусом <span class="label label-info">Только на заказ</span>');
}

$this->pageTitle = Yii::app()->name.' - Магазин - Товары';

Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.fontello').'.css'));

if(Yii::app()->user->checkAccess('product.create')) {
    $this->menuItem = array('label'=>'Создать товар', 'url'=>array('/product/create'));
}

$template = $this->renderPartial('_template', array(
    'model'=>$model,
    'type'=>$type,
), true);
if($type === 'table') {
    $view = '_view_list';
} else {
    $view = '_view_grid';
}
$this->widget('local.widgets.TbSortedListView', array(
    'itemView'=>$view,
    'dataProvider'=>$dataProvider,
    'template'=>$template,
    'sortableAttributes'=>array(
        'status',
        'price',
        'title'
    ),
    'sorterCssClass'=>'sorter pull-right',
    'ascSortIcon'=>'arrow-down',
    'descSortIcon'=>'arrow-up',
    'htmlOptions'=>array('id'=>'product-list'),
    'summaryText'=>'Товары с {start} по {end} из {count}',
    'summaryCssClass'=>'',
    'afterAjaxUpdate'=>'registerForms',
));
?>