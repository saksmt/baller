<div class="panel search-form" hidden="hidden">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'type'=>'horizontal',
    ));/** @var TbActiveForm $form */ ?>
    <?php
    $materials = CHtml::listData(Materials::model()->findAll(), 'id','name');
    $materials = CMap::mergeArray(array(null=>'Не важно'),$materials);
    ?>
    <fieldset>
        <legend>Расширенный поиск</legend>
        <?= $form->textFieldRow($model,'title',array('class'=>'span5'));?>
        <div class="control-group">
            <?= $form->label($model,'width',array('class'=>'control-label'))?>
            <div class="controls controls-row">
                <div class="input-append input-prepend span5">
                    <span class="add-on">От: </span>
                    <?= $form->textField($model,'width',array('class'=>'span5'))?>
                    <span class="add-on">До: </span>
                    <?= $form->textField($model,'widthTo',array('class'=>'span5'))?>
                    <span class="add-on">мм.</span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <?= $form->label($model,'height',array('class'=>'control-label'))?>
            <div class="controls controls-row">
                <div class="input-append input-prepend span5">
                    <span class="add-on">От: </span>
                    <?= $form->textField($model,'height',array('class'=>'span5'))?>
                    <span class="add-on">До: </span>
                    <?= $form->textField($model,'heightTo',array('class'=>'span5'))?>
                    <span class="add-on">мм.</span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <?= $form->label($model,'length',array('class'=>'control-label'))?>
            <div class="controls controls-row">
                <div class="input-append input-prepend span5">
                    <span class="add-on">От: </span>
                    <?= $form->textField($model,'length',array('class'=>'span5'))?>
                    <span class="add-on">До: </span>
                    <?= $form->textField($model,'lengthTo',array('class'=>'span5'))?>
                    <span class="add-on">мм.</span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <?= $form->label($model,'price',array('class'=>'control-label'))?>
            <div class="controls controls-row">
                <div class="input-append input-prepend span5">
                    <span class="add-on">От: </span>
                    <?= $form->textField($model,'price',array('class'=>'span5'))?>
                    <span class="add-on">До: </span>
                    <?= $form->textField($model,'priceTo',array('class'=>'span5'))?>
                    <span class="add-on">руб.</span>
                </div>
            </div>
        </div>
        <?= $form->dropdownListRow($model,'category_id',CHtml::listData(Categories::getAllFree(true),'id','name'),array('class'=>'span5'));?>
        <?= $form->dropdownListRow($model,'material_id',$materials,array('class'=>'span5'));?>
        <?= $form->dropdownListRow($model,'status',Product::getStatuses(true),array('class'=>'span5'));?>
        <?= $form->textAreaRow($model,'description',array('class'=>'span5'));?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'type'=>'primary',
            'icon'=>'search',
            'label'=>'Поиск',
            'buttonType'=>'submit',
            'htmlOptions'=>array(
                'id'=>'searchButton',
            ),
        ));?>
    </div>
    <?php $this->endWidget();?>
</div>