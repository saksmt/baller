<?php
/**
 * @var ShopController $this
 * @var string $type
 * @var Product $model
 */
?>
<div class="panel search-panel">
    <div class="span5">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array('type'=>'search','htmlOptions'=>array('class'=>'short-search-form')));?>
        <div class="input-append">
            <?= $form->textField($model,'title',array('class'=>'search-query'));?>
            <button class="btn" type="submit"><i class="icon icon-search"></i> Поиск</button>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'button',
                'label'=>'Расширенный поиск',
                'icon'=>'cog',
                'htmlOptions'=>array('class'=>'search-button'),
            )); ?>
        </div>
        <?php $this->endWidget();?>
    </div>
    <div class="span7 pull-right">
        <div class="span3 pull-right">
            <div class="btn-group">
                <?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
                    'toggle'=>'radio',
                    'buttons'=>array(
                        array('buttonType'=>'button','label'=>'Вид: ','htmlOptions'=>array('disabled'=>'disabled')),
                        array('icon'=>'th','url'=>array('shop/products/grid'),'htmlOptions'=>array('class'=>$type=='table'?:'active')),
                        array('icon'=>'th-list','url'=>array('shop/products/table'),'htmlOptions'=>array('class'=>$type=='table'?'active':'')),
                    ),
                ));?>
            </div>
        </div>
        <div class="span9 pull-right" id="sorter">
            {sorter}
        </div>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('search', 'registerForms("product-list");');
?>

<?php $this->renderPartial('_search',array('model'=>$model));?>
<div id="product-list-content">
    <div class="text-center first-pagination">
        {pager}
    </div>
    <div class="product-list-items">
        <?= $type==='table'?'':'<ul class="thumbnails">';?>
            {items}
        <?= $type==='table'?'':'</ul>';?>
    </div>
    <div class="text-center">{pager}</div>
</div>