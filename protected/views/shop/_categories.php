<?php
foreach ($categories as $category){
    echo CHtml::openTag('li', array('class'=>isset($class)?$class:''));
    echo CHtml::link($category->name, array('category','id'=>$category->id));
    echo CHtml::closeTag('li');
}