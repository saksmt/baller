<?php
$this->pageTitle = Yii::app()->name.' - Магазин - Категории';
$this->breadcrumbs=array();
$back = $model;
if (isset($model) && !is_array($model)) {
    $this->breadcrumbs[] = $model->name;
    $back = $model->parent;
}
while(true) {
    if (!isset($back) || is_array($model)) {
        if (empty($this->breadcrumbs)) {
            $this->breadcrumbs[] = 'Список категорий';
        } else {
            $this->breadcrumbs['Список категорий'] = array('categories');
        }
        $this->breadcrumbs['Магазин'] = array('index');
        break;
    }
    $this->breadcrumbs[$back->name] = array('index', 'id' => $back->id);
    $back = $back->parent;
}
$this->breadcrumbs = array_reverse($this->breadcrumbs);

?>
<!--<h1><?= is_array($model)?'Категории':$model->name; ?></h1>-->
<ul class="panel unstyled">
    <?php $this->renderPartial('_categories',array('categories'=>is_array($model)?$model:$model->children,'class'=>'h4'));?>
</ul>