<?php
$image = Yii::app()->baseUrl.'/images/none.png';
Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.grid').'.css'));
if(isset($data->images)) {
    $image = Yii::app()->baseUrl.'/images/Product/'.unserialize($data->images)[0];
}
$description = $data->description;
if(strlen($description) > 200) {
    $description = substr($description,0,100).'...';
}
?>
<li class="col-3 padd-1-1">
    <div class="thumbnail product-in-grid">
        <div>
            <div class="product-image" style="height:100px; margin:0 auto; background-image:url('<?php echo $image;?>');"></div>
            <h3 class="text-center"><?= CHtml::link($data->title.'',array('shop/product','id'=>$data->id)); ?></h3>
            <p class="text-center product-status"><span class="label label-<?= $data->statusAsLabelClass; ?>"><?= $data->userStatus; ?></span></p>
            <div style="padding: 0 10px;">
                <p><?= $description ?></p>
                <p><strong>Высота: </strong><span class="pull-right"><?= $data->width;?>мм.</span></p>
                <p><strong>Ширина: </strong><span class="pull-right"><?= $data->height;?>мм.</span></p>
                <p><strong>Глубина: </strong><span class="pull-right"><?= $data->length;?>мм.</span></p>
                <p><strong>Артикул: </strong><span class="pull-right"><?php printf('%05d',$data->id); ?></span></p>
                <p><strong>Цена: </strong><span class="pull-right"><?= $data->price; ?><i class="icon-rub"></i></span></p>
            </div>
        </div>
        <p class="text-center grid-to-cart"><?php $this->widget('bootstrap.widgets.TbButton', array(
                'type'=>'success',
                'buttonType'=>'ajaxButton',
                'icon'=>'shopping-cart',
                'url'=>array('shop/addToCart','id'=>$data->id),
                'label'=>'В корзину',
                'disabled'=>!$data->isAvailable(),
                'ajaxOptions'=>array('update'=>'#alerts'),
            ));?></p>
    </div>
</li>
