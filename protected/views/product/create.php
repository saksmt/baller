<?php
$this->breadcrumbs=array(
	'Товары'=>array('index'),
	'Создание',
);

$this->pageTitle = Yii::app()->name.' - Создание товара';

?>

<h1>Создать товар</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>