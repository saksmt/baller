<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.fontello').'.css')); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Поля помеченные "<span class="required">*</span>" обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>50)); ?>

    <?php echo $form->textFieldRow($model,'price',array('class'=>'text-right', 'append'=>'<i class="icon-rub"></i>')); ?>

	<?php echo $form->textFieldRow($model,'width',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>

	<?php echo $form->textFieldRow($model,'height',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>

	<?php echo $form->textFieldRow($model,'length',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>

    <?php echo $form->dropdownListRow($model,'category_id',CHtml::listData(Categories::getAllFree(), 'id', 'name'), array('class'=>'span5'))?>

<?php echo $form->dropdownListRow($model,'material_id',CHtml::listData(Materials::model()->findAll(), 'id', 'name'), array('class'=>'span5'))?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

	<?php echo $form->dropdownListRow($model,'status',$model->statuses,array('class'=>'span5')); ?>

    <?php echo $form->labelEx($model,'images'); ?>
	<?php $this->widget('local.widgets.TbMultiFileUpload', array(
        'model'=>$model,
        'attribute'=>'images',
    )); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
