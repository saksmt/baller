<?php
$this->breadcrumbs=array(
	'Товары',
);

$this->pageTitle = Yii::app()->name.' - Товары';

?>

<h1>Товары</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
