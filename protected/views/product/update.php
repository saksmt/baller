<?php
$this->breadcrumbs=array(
	'Товары'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Изменение',
);

$this->pageTitle = Yii::app()->name.' - Изменение товара';

$this->menu=array(
	array('label'=>'Список Товара','url'=>array('index')),
	array('label'=>'Создание Товара','url'=>array('create')),
	array('label'=>'Просмотр Товара','url'=>array('view','id'=>$model->id)),
	array('label'=>'Администрирование Товара','url'=>array('admin')),
);
?>

<h1>Изменение товара "<?php echo $model->title; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>