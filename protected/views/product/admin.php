<?php
$this->breadcrumbs=array(
	'Товары'=>array('index'),
	'Администрирование',
);

$this->pageTitle = Yii::app()->name.' - Администрирование товаров';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('product-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Администрирование товаров</h1>

<p>
Вы можете добавить оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начало каждого поля для определения способа сравнения.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'title',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по названию',
            ),
        ),
        array(
            'name'=>'width',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по ширине',
            ),
        ),
        array(
            'name'=>'height',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по высоте',
            ),
        ),
        array(
            'name'=>'length',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по глубине',
            ),
        ),
        array(
            'name'=>'material_id',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по материалу'
            ),
            'value'=>'isset($data->material)?$data->material->name:"Нет"',
            'filter'=>CHtml::listData(Materials::model()->findAll(), 'id', 'name'),
        ),
        array(
            'name'=>'category_id',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по категории'
            ),
            'value'=>'isset($data->category)?$data->category->name:"Нет"',
            'filter'=>CHtml::listData(Categories::model()->findAll(), 'id', 'name'),
        ),
        array(
            'name'=>'description',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по описанию',
            ),
        ),
        array(
            'name'=>'status',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по статусу'
            ),
            'value'=>'$data->getStatuses()[$data->status]',
            'filter'=>Product::getStatuses(),
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'header'=>'Действия',
        ),
	),
)); ?>
