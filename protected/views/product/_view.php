<?php
$image = Yii::app()->baseUrl.'/images/none.png';
if(isset($data->images)) {
    $image = Yii::app()->baseUrl.'/images/Product/'.unserialize($data->images)[0];
}
$description = $data->description;
if(strlen($description) > 200) {
    $description = substr($description,0,200).'...';
}
?>
<div class="panel">
    <div class="row-fluid">
        <div class="span4">
            <div class="thumbnail" style="height:200px; margin:0 auto; background-image:url('<?php echo $image;?>');background-repeat:no-repeat;background-size:contain;background-position:center;">
            </div>
        </div>
        <div class="span8 pull-right">
            <div class="span10">
                <h3><?= CHtml::link($data->title,array('view',array('id'=>$data->id))); ?></h3>
                <p class="lead"><?= $description ?></p>
                <p><strong>Высота: </strong><?= $data->width;?>мм.</p>
                <p><strong>Ширина: </strong><?= $data->height;?>мм.</p>
                <p><strong>Глубина: </strong><?= $data->length;?>мм.</p>
            </div>
            <div class="span2">
                <p><?= $data->price; ?>руб.</p>
            </div>
        </div>
    </div>
</div>