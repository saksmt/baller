<?php

$this->breadcrumbs=array(
	'Товары'=>array('index'),
	$model->title,
);

$this->pageTitle = Yii::app()->name.' - Просмотр товара';

?>

<h1>Просмотр товара "<?php echo $model->title; ?>"</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'width',
		'height',
		'length',
		'category_id',
		'material_id',
		'description',
		'status',
		'images',
	),
)); ?>
