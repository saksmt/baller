<?php
$contents = CHtml::encode($data->contents);
if(strlen($contents) > 50) {
    $contents = substr($contents, 0, 300).CHtml::link('...', array('view', 'id'=>$data->id));
}
?>
<div class="view panel">

    <h2><?php echo CHtml::link(CHtml::encode($data->title), array('view','id'=>$data->id));?></h2>
    <hr>
    <div class="row-fluid">
        <?php if($data->image): ?>
            <div class="thumbnail span8">
                <img class src="<?php echo Yii::app()->baseUrl.'/images/News/'.$data->image;?>">
            </div>
        <?php endif; ?>
        <p class="<?php echo $data->image?'span4':''; ?>"><?php echo $contents;?></p>
    </div>
    <div class="row-fluid">
        <small class="text-right span12"><?php echo CHtml::encode(date('d.m.y',strtotime($data->date)));?></small>
    </div>

</div>