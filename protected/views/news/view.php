<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	$model->title,
);

$this->pageTitle = Yii::app()->name.' - Просмотр новости';

?>

<h2>Просмотр новости "<?php echo $model->title; ?>"</h2>
<hr>
<div class="row-fluid">
    <?php if($model->image):?>
        <div class="thumbnail pull-left left-image">
            <img class="" src="<?php echo Yii::app()->baseUrl.'/images/News/'.$model->image;?>">
        </div>
    <?php endif;?>
    <span class="<?php echo ($model->image)?'':'';?>">
        <?php echo $model->contents;?>
    </span>
</div>
<p class="text-center"><?php echo '<strong>Дата публикации</strong>: '.date('d.m.y',strtotime($model->date));?></p>