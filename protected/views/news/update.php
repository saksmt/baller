<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Изменение',
);

$this->pageTitle = Yii::app()->name.' - Изменение новости';
?>

<h1>Изменение новости "<?php echo $model->title; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>