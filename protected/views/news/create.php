<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	'Создание',
);

$this->pageTitle = Yii::app()->name.' - Создание новости';
?>

<h1>Создать новость</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>