<?php
$this->breadcrumbs=array(
	'Новости',
);

$this->pageTitle = Yii::app()->name.' - Новости';
?>

<h1>Новости</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
