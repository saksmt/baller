<?php
$this->breadcrumbs=array(
	'Новости'=>array('index'),
	'Администрирование',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('news-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/row_strip.js');
Yii::app()->clientScript->registerScript('strip', '$(".news-contents").each(function(){'
    .'var $this = $(this);'
    .'stripRow($this);'
    .'});');
$this->pageTitle = Yii::app()->name.' - Администрирование новостей';
?>

<h1>Администрирование новостей</h1>

<p>
Вы можете добавить оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начало каждого поля для определения способа сравнения.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'title',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по заголовку',
            ),
        ),
		array(
            'name'=>'contents',
            'htmlOptions'=>array(
                'class'=>'news-contents',
            ),
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по содержимому',
            ),
        ),
        array(
            'name'=>'date',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по дате',
            ),
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'header'=>'Действия',
		),
	),
    'template'=>'<div class="row-fluid"><div class="span6 cv">{pager}</div><div class="span6 cv">{summary}</div></div><div>{items}</div><div class="text-center">{pager}</div>',
    'type'=>'hover',
    'summaryText'=>'Страница {page} из {pages}',
    'selectionChanged'=>'js:stripSelectionChanged',
)); ?>
