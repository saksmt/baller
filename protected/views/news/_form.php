<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/fileform.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/date.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/datepicker.css');
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
));
?>

	<p class="help-block">Поля помеченные "<span class="required">*</span>" обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'contents',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

    <?php echo $form->label($model,'date',array('id'=>'dateLabel'));?>

    <div class="input-append date">
        <input type="text" class="span5" data-date-format="dd.mm.yyyy">
        <span class="add-on"><i class="icon-calendar"></i></span>
    </div>
    <?php echo $form->textField($model,'date',array('class'=>'realInput hidden')); ?>

    <?php if($model->image): ?>
        <div class="thumbnail">
            <img class src="<?php echo Yii::app()->baseUrl.'/images/News/'.$model->image;?>">
        </div>
    <?php endif; ?>
    <?php echo $form->fileFieldRow($model,'image',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
