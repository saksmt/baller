<?php
$this->pageTitle = Yii::app()->name.' - Профиль';
?>
<div class="panel">
    <p class="lead"><?= $user->name; ?></p>
    <p><?= $user->email ?></p>
    <?php
    if (isset($user->login)) {
        echo '<p>'.$user->login.'</p>';
    }
    ?>
</div>