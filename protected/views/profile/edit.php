<div class="span12">
    <div class="alert alert-info">
        Поля помеченные <span class="required">*</span> обязательны для заполнения.
    </div>
    <?php
    /**
     * @var $this ProfileController
     * @var $model UserEditForm
     */

    /// @var $form TbActiveForm
    $this->breadcrumbs = array('Редактирование профиля');
    $this->pageTitle = Yii::app()->name.' - Редактирование профиля';
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'verticalForm',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'well'),
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'focus'=>array($model, 'login'),
    ));
    $form->errorSummary($model);
    ?>
    <div class="row-fluid"><?= $form->textFieldRow($model, 'login', array('class'=>'span12', 'placeholder'=>'Логин')); ?></div>
    <div class="row-fluid"><?= $form->textFieldRow($model, 'email', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?= $form->passwordFieldRow($model, 'passwd', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?= $form->passwordFieldRow($model, 'passwdRepeat', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?= $form->textFieldRow($model, 'name', array('class'=>'span12')); ?></div>
    <div class="row-fluid">
        <?= $form->passwordFieldRow($model, 'password', array('class' => 'span12')); ?>
        <p class="text-error text-center">
            Для сохранения необходимо ввести ваш текущий пароль!
        </p>
    </div>
    <div class="row-fluid">
        <div class="span12 text-center">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>'Сохранить',
            ));
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>