
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'products-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'width',array('class'=>'span5','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'height',array('class'=>'span5','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'length',array('class'=>'span5','maxlength'=>4)); ?>

	<?php echo $form->textFieldRow($model,'category',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textFieldRow($model,'subcategory',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textFieldRow($model,'material',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>13)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
