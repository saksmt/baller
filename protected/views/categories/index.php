<?php

$this->breadcrumbs=array(
);

$back = $parent;
if (isset($parent)) {
    $this->breadcrumbs[] = $parent->name;
    $back = $parent->parent;
}
while(true) {
    if (!isset($back)) {
        if (empty($this->breadcrumbs)) {
            $this->breadcrumbs[] = 'Категории';
        } else {
            $this->breadcrumbs['Категории'] = array('index');
        }
        break;
    }
    $this->breadcrumbs[$back->name] = array('index', 'id' => $back->id);
    $back = $back->parent;
}
$this->breadcrumbs = array_reverse($this->breadcrumbs);

$this->pageTitle = Yii::app()->name.' - '.(isset($parent)?'Подкатегории "'.$parent->name.'"':'Категории');

?>

<h1><?php echo (isset($parent)?'Подкатегории "'.
        CHtml::link($parent->name,array('view','id'=>$parent->id)).
        '"'
        :'Категории'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
