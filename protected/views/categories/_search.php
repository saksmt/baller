<br><?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions'=>array('class'=>'panel'),
)); ?>

    <?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

    <?php echo $form->dropdownListRow($model,'parent_id',CHtml::listData(Categories::model()->findAll(),'id','name')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Поиск',
        )); ?>
    </div>

<?php $this->endWidget(); ?>
