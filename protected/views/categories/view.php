<?php
$this->breadcrumbs=array(
	'Категории'=>array('index'),
	$model->name,
);

$model->path = $this->route;

$this->pageTitle = Yii::app()->name.' - Просмотр Категории';

?>

<h1>Просмотр категории "<?php echo $model->name; ?>"</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'name',
        array(
            'label'=>$model->attributeLabels()['parent_id'],
            'type'=>'html',
            'value'=>(isset($model->parent)?CHtml::link($model->parent->name, array('view', 'id'=>$model->parent->id)):'<span class="text-error">Нет</span>'),
        ),
        'childrenList:html',
	),
)); ?>
