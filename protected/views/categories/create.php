<?php
$this->breadcrumbs=array(
	'Категории'=>array('index'),
	'Создание',
);

$this->pageTitle = Yii::app()->name.' - Создание Категории';

?>

<h1>Создать категорию</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>