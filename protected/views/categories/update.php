<?php
$this->breadcrumbs=array(
	'Категории'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Изменение',
);

$this->pageTitle = Yii::app()->name.' - Изменение Категории';

?>

<h1>Изменение категории "<?php echo $model->name; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>