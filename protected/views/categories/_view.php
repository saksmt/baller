<div class="view">

    <div class="panel">
        <h4><?php echo CHtml::link($data->name, array('view','id'=>$data->id))?></h4>
        <?php if (!empty($data->children)): ?>
            <p><?php echo CHtml::link('Подкатегории', array('index', 'id'=>$data->id)) ?></p>
        <?php endif; ?>
    </div>

</div>