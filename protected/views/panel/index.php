<?php
/** @var $this Controller */
?>

<?php $this->pageTitle = Yii::app()->name.' - Административная панель'; ?>

<h1>
    Выберите секцию сайта для управления
</h1>

<div class="well">

    <p class="lead">
        <?php
        echo CHtml::link('Материалы', array('/materials'));
        ?>
    </p>

    <p class="lead">
        <?php
        echo CHtml::link('Галерея', array('/gallery'));
        ?>
    </p>

    <p class="lead">
        <?php
        echo CHtml::link('Новости', array('/news'));
        ?>
    </p>

    <p class="lead">
        <?php
        echo CHtml::link('Категории', array('/categories'));
        ?>
    </p>

    <p class="lead">
        <?php
        echo CHtml::link('Пользователи', array('/user/admin'));
        ?>
    </p>

    <p class="lead">
        <?php
        echo CHtml::link('Товары', array('/product'));
        ?>
    </p>