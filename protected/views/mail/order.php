<?php
/**
 * @var Order $order
 */

$logo = 'url("data:image/png;base64,'.base64_encode(file_get_contents(Yii::getPathOfAlias('webroot.images.logoWhite').'.png')).'");';
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= Yii::app()->name?> - Регистрация</title>
        <meta charset="utf-8">
        <style type="text/css">
            /*<![CDATA[*/
            body {
                padding: 0;
                margin: 0;
            }
            .logo {
                background-color: transparent;
                background-image: <?php echo $logo;?>;
                background-position: center center;
                background-size: 100%;
                background-repeat: no-repeat;
                display: block;
                margin: 0 auto;
                width: 45px;
                height: 36px;
                padding-top: 2px;
                padding-bottom: 2px;
            }
            .menu {
                width: 100%;
                height: 40px;
                background-color: #000000;
            }
            .content {
                border: 1px solid #E3E3E3;
                border-radius: 5px;
                background-color: #FAFAFA;
                padding: 19px;
                min-height: 20px;
                margin: 20px;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 20px;
                color: #333;
            }
            strong {
                font-weight: bold;
            }
            a:hover, a:focus {
                color: #005580;
                text-decoration: underline;
            }
            a:focus {
                outline: thin dotted #333;
                outline-offset: -2px;
            }
            a {
                color: #08C;
                text-decoration: none;
            }
            ul {
                padding: 0;
                margin: 0 0 10px 25px;
            }
            li {
                line-height: 20px;
            }
            /*]]>*/
        </style>
    </head>
    <body>
        <div class="menu">
            <a class="logo" href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"></a>
        </div>
        <div class="content">
            <p>Здравствуйте, <?= $order->user->name ?></p>
            <p>Спасибо за выбор нашего магазина. Ниже представлены данные по вашему заказу.</p>
            <ul>
                <li><strong>Номер заказа: </strong><?= $order->id; ?></li>
                <li><strong>Название заказа: </strong><?= $order->name; ?></li>
                <li><strong>Ваш контактный номер: </strong><?= $order->telephone; ?></li>
                <li><strong>Дата оформления: </strong><?= date('d.m.y H:i:s',strtotime($order->date)); ?></li>
                <li><strong>Комментарий к заказу: </strong><?= $order->comment; ?></li>
                <li><strong>Ваш контактный номер: </strong><?= $order->telephone; ?></li>
                <?php if (isset($order->coord_x)): ?>
                    <li><strong>Координаты доставки: </strong><?= $order->coord_x; ?>; <?= $order->coord_y; ?></li>
                <?php endif; ?>
                <li><strong>Суммарная стоимость: </strong><?= $order->cost; ?> руб.(Без учёта стоимости изменённых товаров, если они были)</li>
            </ul>
            <p>Более подробную информацию, включая список товаров и статус заказа, вы можете просмотреть на <a href="<?= Yii::app()->createAbsoluteUrl('/'); ?>">сайте</a>
                в разделе <a href="<?= Yii::app()->createAbsoluteUrl('/orders/show',array('id'=>$order->id)); ?>">"Мои заказы"</a></p>
        </div>
    </body>
</html>