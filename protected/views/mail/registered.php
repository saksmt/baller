<?php
/**
 * @var RegisterForm $user
 */

$logo = 'url("data:image/png;base64,'.base64_encode(file_get_contents(Yii::getPathOfAlias('webroot.images.logoWhite').'.png')).'");';

?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= Yii::app()->name?> - Регистрация</title>
        <meta charset="utf-8">
        <style type="text/css">
            /*<![CDATA[*/
            body {
                padding: 0;
                margin: 0;
            }
            .logo {
                background-color: transparent;
                background-image: <?php echo $logo;?>;
                background-position: center center;
                background-size: 100%;
                background-repeat: no-repeat;
                display: block;
                margin: 0 auto;
                width: 45px;
                height: 36px;
                padding-top: 2px;
                padding-bottom: 2px;
            }
            .menu {
                width: 100%;
                height: 40px;
                background-color: #000000;
            }
            .content {
                border: 1px solid #E3E3E3;
                border-radius: 5px;
                background-color: #FAFAFA;
                padding: 19px;
                min-height: 20px;
                margin: 20px;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 20px;
                color: #333;
            }
            strong {
                font-weight: bold;
            }
            a:hover, a:focus {
                color: #005580;
                text-decoration: underline;
            }
            a:focus {
                outline: thin dotted #333;
                outline-offset: -2px;
            }
            a {
                color: #08C;
                text-decoration: none;
            }
            ul {
                padding: 0;
                margin: 0 0 10px 25px;
            }
            li {
                line-height: 20px;
            }
            /*]]>*/
        </style>
    </head>
    <body>
        <div class="menu">
            <a class="logo" href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"></a>
        </div>
        <div class="content">
            <p>Здравствуйте, <?= $user->name; ?></p>
            <p>Спасибо за регистрацию на сайте <a href="<?php echo Yii::app()->createAbsoluteUrl('/');?>">Баллер</a>.
                Если вы не проходили регистрацию, проигнорируйте это письмо.</p>
            <p>Ваши регистрационные данные:</p>
            <ul>
                <li><strong>ФИО: </strong><?= $user->name; ?></li>
                <li><strong>Email: </strong><?= $user->email; ?></li>
                <?php if(isset($user->login) && !empty($user->login)): ?>
                    <li><strong>Логин: </strong><?= $user->login; ?></li>
                <?php endif; ?>
            </ul>
            <p>Для активации аккаунта перейдите по <a href="<?= Yii::app()->createAbsoluteUrl('user/activate', array('securecode'=>$user->getCode(),'id'=>$user->id)); ?>">ссылке</a></p>
        </div>
    </body>
</html>