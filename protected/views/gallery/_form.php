<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/fileform.js'); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'gallery-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Поля помеченные "<span class="required">*</span>" обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>100)); ?>

    <?php if($model->image): ?>
        <div class="thumbnail">
            <img src="<?php echo Yii::app()->baseUrl.'/images/Gallery/'.$model->image;?>">
        </div>
    <?php endif; ?>
    <?php echo $form->fileFieldRow($model,'image',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
