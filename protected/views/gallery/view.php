<?php
$this->breadcrumbs=array(
	'Галерея'=>array('index'),
	$model->title,
);

$this->pageTitle = Yii::app()->name.' - Просмотр Галерея';

?>

<h1>Просмотр элемента галереи "<?php echo $model->title; ?>"</h1>

<?php $this->renderPartial('_view', array('data'=>$model)); ?>
