<div class="view">

    <h3><?= CHtml::link($data->title,$data->link); ?></h3>
    <div class="thumbnail">
        <img src="<?= Yii::app()->baseUrl.'/images/Gallery/'.$data->image; ?>">
    </div>

</div>