<?php
$this->breadcrumbs=array(
	'Галерея'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Изменение',
);

$this->pageTitle = Yii::app()->name.' - Изменение элемента галереи';

?>

<h1>Изменение элемента галереи "<?php echo $model->title; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>