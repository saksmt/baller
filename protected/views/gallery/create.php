<?php
$this->breadcrumbs=array(
	'Галерея'=>array('index'),
	'Создание элемента',
);

$this->pageTitle = Yii::app()->name.' - Создание элемента галереи';

?>

<h1>Создать элемент галереи</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>