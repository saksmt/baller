<?php
$this->breadcrumbs=array(
	'Галерея',
);

$this->pageTitle = Yii::app()->name.' - Галерея';

?>

<h1>Галерея</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
