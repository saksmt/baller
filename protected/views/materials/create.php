<?php
$this->breadcrumbs=array(
	'Материалы'=>array('index'),
	'Создание',
);

$this->pageTitle = Yii::app()->name.' - Создание Материала';

?>

<h1>Создать материал</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>