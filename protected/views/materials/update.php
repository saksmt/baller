<?php
$this->breadcrumbs=array(
	'Материалы'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Изменение',
);

$this->pageTitle = Yii::app()->name.' - Изменение материала';

?>

<h1>Изменение материала "<?php echo $model->name; ?>"</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>