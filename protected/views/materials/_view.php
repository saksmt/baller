<?php Yii::app()->clientScript->registerScript('image','$(function(){$(".material-image").height($(".material-image").width()-10)});');?>

<div class="view panel">

    <h2><?php echo CHtml::link(CHtml::encode($data->name), array('view','id'=>$data->id));?></h2>
	<hr>

    <div class="row-fluid">
        <?php if($data->image): ?>
            <div class="thumbnail span2">
                <div class="material-image" style="background-image:url(<?php echo Yii::app()->baseUrl.'/images/Materials/'.$data->image;?>);"></div>
            </div>
        <?php endif; ?>
        <div class="<?php echo $data->image?'span10':''; ?>">
            <p>
                <strong><?php echo $data->getAttributeLabel('name')?>: </strong><?php echo CHtml::encode($data->name); ?>
            </p>
            <p>
                <strong><?php echo $data->getAttributeLabel('code')?>: </strong><?php echo CHtml::encode($data->code); ?>
            </p>
            <p>
                <strong><?php echo $data->getAttributeLabel('comment')?>: </strong><?php echo CHtml::encode($data->comment); ?>
            </p>
        </div>
    </div>


</div>