<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/fileform.js'); ?>
<?php Yii::app()->clientScript->registerScript('image','$(function(){$(".material-image").height($(".material-image").width()-10)});');?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'materials-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Поля помеченные "<span class="required">*</span>" обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'comment',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>255)); ?>

    <?php if($model->image): ?>
        <div class="clearfix">
            <div class="thumbnail span2">
                <div class="material-image" style="background-image:url(<?php echo Yii::app()->baseUrl.'/images/Materials/'.$model->image;?>);"></div>
            </div>
        </div>
    <?php endif; ?>
    <?php echo $form->fileFieldRow($model,'image',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
