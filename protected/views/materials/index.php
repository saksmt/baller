<?php
$this->breadcrumbs=array(
	'Материалы',
);

$this->pageTitle = Yii::app()->name.' - Материалы';

?>

<h1>Материалы</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
