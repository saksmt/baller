<?php Yii::app()->clientScript->registerScript('image','$(function(){$(".material-image").height($(".material-image").width()-10)});');?>
<?php
$this->breadcrumbs=array(
	'Материалы'=>array('index'),
	$model->name,
);

$this->pageTitle = Yii::app()->name.' - Просмотр материала';

?>

<h1>Просмотр материала "<?php echo $model->name; ?>"</h1>
<div class="row-fluid">
    <?php if($model->image):?>
            <div class="span3">
                <div class="thumbnail">
                    <div class="material-image" style="background-image:url(<?php echo Yii::app()->baseUrl;?>/images/Materials/<?php echo $model->image; ?>);"></div>
                </div>
            </div>
            <div class="span9">
        <?php endif;?>
        <?php $this->widget('bootstrap.widgets.TbDetailView',array(
            'data'=>$model,
            'attributes'=>array(
                'name',
                'comment',
                'code',
            ),
        )); ?>
    <?php if($model->image)echo '</div>'; ?>
</div>
