<div class="span6">
    <div class="alert alert-info">
        Поля помеченные <span class="required">*</span> обязательны для заполнения.
    </div>
    <?php
    /**
     * @var $this UserController
     * @var $model RegisterForm
     */

    /// @var $form TbActiveForm
    $this->breadcrumbs = array('Регистрация');
    $this->pageTitle = Yii::app()->name.' - Регистрация';
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'verticalForm',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'panel'),
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'focus'=>array($model, 'login'),
    ));
    $form->errorSummary($model);
    ?>
    <div class="row-fluid"><?php echo $form->textFieldRow($model, 'login', array('class'=>'span12', 'placeholder'=>'Логин')); ?></div>
    <div class="row-fluid"><?php echo $form->textFieldRow($model, 'email', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->passwordFieldRow($model, 'passwd', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->passwordFieldRow($model, 'passwd_repeat', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->textFieldRow($model, 'name', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->textFieldRow($model, 'captcha', array(
            'class'=>'span12',
        )); ?></div>
    <div class="row-fluid">
        <div class="control-group">
            <div class="control-label">Код:</div>
            <div class="controls">
                <a class="thumbnail" style="height: 20px; overflow: hidden;" rel="tooltip" data-title="Нажмите для генерации новой картинки">
                    <?php $this->widget('CCaptcha', array(
                        'clickableImage'=>true,
                        'showRefreshButton'=>false,
                        'imageOptions'=>array(
                            'style'=>'margin-top: -7.5px;'
                        ),
                    ));?>
                </a>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <p class="help-block">
            Продолжая процедуру регистрации, вы соглашаетесь с <?= CHtml::link('условиями использования',array('/page/terms'));?>.
        </p><br/>
        <div class="span12 text-center">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>'Зарегистрироваться',
            ));
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<div class="span6 panel">
    <strong>Какие преимущества даёт регистрация?</strong>
    <ul>
        <li>Оформление мебели <em>"под заказ"</em></li>
        <li>Возможность отслеживать статус оформления заказа</li>
        <li>...</li>
    </ul>
</div>