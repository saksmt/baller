<?php
$this->breadcrumbs=array(
    'Административная панель'=>array('/panel'),
    'Управление пользователями',
);

$this->pageTitle = Yii::app()->name.' - Управление пользователями';

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/users.js');
Yii::app()->clientScript->registerScript('statuses', 'var statuses = '.CJavaScript::encode(CHtml::dropDownList('UserDataModel[role]',0,UserDataModel::getStatuses())).';', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('submitChange', '$("#userDataModalSubmit").click(function(){submitChanges("'.$this->createUrl('update').'");return false;});');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('categories-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'userDataModal'));?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Пользователь "<span id="userName"></span>"</h4>
    </div>
    <div class="modal-body">
        <div id="userLoading" class="grid-view-loading">
        </div>
        <div class="data"></div>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Сохранить',
            'url'=>'#',
            'type'=>'primary',
            'htmlOptions'=>array(
                'id'=>'userDataModalSubmit',
            ),
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Закрыть',
            'url'=>'#',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget(); ?>

<h1>Администрирование пользователей</h1>

<p>
    Вы можете добавить оператор сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    или <b>=</b>) в начало каждого поля для определения способа сравнения.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'categories-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'name'=>'login',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по имени пользователя'
            ),
        ),
        array(
            'name'=>'email',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по email'
            ),
        ),
        array(
            'name'=>'name',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по имени'
            ),
        ),
        array(
            'name'=>'role',
            'filterWrapTag'=>'a',
            'filterWrapHtmlOptions'=>array(
                'rel'=>'tooltip',
                'title'=>'Поиск по статусу'
            ),
            'value'=>'UserDataModel::roleToStatus($data->role)',
            'filter'=>UserDataModel::getStatuses(),
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'header'=>'Действия',
            'viewButtonUrl'=>'"javascript:showUserData(".CJavascript::encode(CMap::mergeArray($data->attributes,array("status"=>UserDataModel::roleToStatus($data->role),"passwd"=>""))).")"',
            'updateButtonUrl'=>'"javascript:showChangePromt(".CJavascript::encode(CMap::mergeArray($data->attributes,array("status"=>UserDataModel::roleToStatus($data->role),"passwd"=>""))).")"',
            'template'=>'{view} {update}',
        ),
    ),
    'template'=>'<div class="row-fluid"><div class="span6 cv">{pager}</div><div class="span6 cv">{summary}</div></div><div>{items}</div><div class="text-center">{pager}</div>',
    'type'=>'hover',
    'summaryText'=>'Страница {page} из {pages}',
)); ?>
