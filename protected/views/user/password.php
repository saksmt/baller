<?php
$this->pageTitle = Yii::app()->name . ' - Восстановление пароля';
$this->breadcrumbs = array(
    'Восстановление пароля'
);
?>

<div class="span6 offset3">
    <form class="form-horizontal panel row-fluid" method="POST">
        <div class="span6 offset3">
            <div class="row-fluid">
                <div class="span4">
                    <label for="email">Email</label>
                </div>
                <div class="span8">
                    <input type="email" name="email" id="email">
                </div>
            </div>
            <p></p>
            <div class="row-fluid">
                <div class="span8 offset4">
                    <button class="btn btn-default">Восстановить</button>
                </div>
            </div>
        </div>
    </form>
</div>