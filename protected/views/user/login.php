<div class="span6">
    <?php
    /**
     * @var UserController $this
     * @var LoginForm $model
     */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'verticalForm',
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'panel'),
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
    ));
    $this->pageTitle = Yii::app()->name.' - Вход';
    $this->breadcrumbs = array('Вход');
    Yii::app()->clientScript->registerCss('noRequired', 'label.required > span.required { '.
                                                            'display:none;'.
                                                        '}');
    ?>

    <div class="row-fluid"><?php echo $form->textFieldRow($model, 'login', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span12')); ?></div>
    <div class="row-fluid"><?php echo $form->checkboxRow($model, 'rememberme', array('class'=>'')); ?></div>
    <div class="row-fluid">
        <div class="span12 text-center">
            <div class="span4 offset4">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'label'=>'Вход',
                    'htmlOptions'=>array('class'=>'span12'),
                )); ?>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'label'=>'Забыли пароль?',
                    'url' => array('user/forgotPassword'),
                    'htmlOptions'=>array('class'=>'span12 btn-link'),
                )); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
<div class="span6">
    <div class="panel">
        <p>Если у вас ещё нет учётной записи, вы можете <?php echo CHtml::link('зарегистрироваться', array('/user/register'));?>.</p>
    </div>
</div>