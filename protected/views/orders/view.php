<?php

/***
 * @var OrdersController $this
 * @var Order $order
 */
$this->pageTitle = Yii::app()->name.' - Просмотр заказа';
$this->breadcrumbs = array(
    'Заказы' => array('index'),
    'Просмотр заказа',
);

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/grid.css');
Yii::app()->clientScript->registerScriptFile('http://api-maps.yandex.ru/2.1/?lang=ru_RU');
if (isset($order->coord_x)) {
    Yii::app()->clientScript->registerScript('pos','var position = ['.$order->coord_x.','.$order->coord_y.'];', CClientScript::POS_HEAD);
}
Yii::app()->clientScript->registerScript('changeUrl','var adminOrderUrl = "'.$this->createUrl('orders/changeStatus',array('id'=>$order->id)).'";', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/ymap/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/view-order.js');

?>

<div class="text-center">
    <div class="panel col-5 padd-1-0 vblock text-left">
        <div class="row-fluid"><strong class="span6 text-left">Номер заказа</strong> <span class="span6 text-right"><?= $order->id; ?></span></div>
        <div class="row-fluid"><strong class="span6 text-left">Название</strong> <span class="span6 text-right"><?= $order->name; ?></span></div>
        <div class="row-fluid"><strong class="span6 text-left">Суммарная стоимость</strong> <span class="span6 text-right"><?= $order->cost; ?></span></div>
        <div class="row-fluid"><strong class="span6 text-left">Дата заказа</strong> <span class="span6 text-right"><?= date('d.m.y H:i:s', strtotime($order->date)); ?></span></div>
        <div class="row-fluid"><strong class="span6 text-left">Контактный номер</strong> <span class="span6 text-right"><?= $order->telephone; ?></span></div>
        <div class="row-fluid"><strong class="span6 text-left">Статус</strong> <span class="span6 text-right"><span class="label label-<?= Order::getStatusClass($order->status); ?>"><span id="status"><?= Order::getStatuses()[$order->status]; ?></span>
        <?php if ($order->id != Yii::app()->user->id) {
            echo ' <a class="icon icon-pencil" rel="tooltip" title="Изменить" href="#" id="changeOrderStatusButton"></a>';
        }?></span></span></div>
        <?php if (Yii::app()->user->checkAccess('orders.admin') && isset($order->user_id)): ?>
            <div class="row-fluid"><strong class="span6 text-left">Пользователь</strong> <span class="text-right span6"><a href="<?= $this->createUrl('/user/view',array('id'=>Yii::app()->user->id)) ?>"><?= Yii::app()->user->name; ?></a></span></div>
        <?php elseif (Yii::app()->user->checkAccess('orders.admin')): ?>
            <div class="row-fluid"><strong class="span6 text-left">Пользователь</strong> <span class="text-right span6">Гость</span></div>
        <?php endif; ?>
    </div>
    <div class="panel col-5 padd-1-0 vblock text-left">
        <h3>Товары:</h3>
        <ul>
            <?php foreach ($order->rel as $product): ?>
                <li><?= $product->custom?'<span class="label label-info">"На заказ!"</span>':''; ?><a href="<?= $this->createUrl('/shop/product',array('id'=>$product->id)); ?>"><?= $product->title; ?></a></li>
            <?php endforeach;?>
        </ul>
    </div>
    <div class="panel col-5 padd-1-0 vblock text-left" id="map">
        Без доставки, с завода.
    </div>
    <div class="hidden panel col-5 padd-1-0 vblock text-left">
        CHATBOX
    </div>
</div>