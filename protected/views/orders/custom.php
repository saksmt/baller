<?php
/**
 * @var OrdersController $this
 * @var Product $model
 */
$this->pageTitle = Yii::app()->name.' - "На заказ!"';
$this->breadcrumbs = array(
    'Магазин'=>array('/shop'),
    '"На заказ!"',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/custom_order.js');
?>
<h1>"На заказ!"</h1>
<div class="panel">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array('type'=>'horizontal')); ?>

    <div class="control-group">
        <label class="control-label">
            Размеры
        </label>
        <div class="controls">
            <div class="btn-group" data-toggle="buttons-radio">
                <button type="button" onclick="hideSizes();" class="btn active">Нужен замер</button>
                <button type="button" onclick="showSizes();" class="btn">Замер не нужен</button>
            </div>
        </div>
    </div>
    <div id="sizes">
        <?= $form->textFieldRow($model,'width',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>

        <?= $form->textFieldRow($model,'height',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>

        <?= $form->textFieldRow($model,'length',array('class'=>'text-right','maxlength'=>4, 'append'=>'мм.')); ?>
    </div>

    <?= $form->dropdownListRow($model,'material_id',CHtml::listData(Materials::model()->findAll(), 'id', 'name'), array('class'=>'span5'))?>
    <div class="form-actions">
        <input type="submit" class="hidden">
        <button class="btn btn-primary" type="button" id="submit">Сохранить и положить в корзину</button>
    </div>
    <?php $this->endWidget();?>
</div>