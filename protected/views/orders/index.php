<?php
/**
 * @var Order $model
 * @var OrdersController $this
 */
$this->pageTitle = Yii::app()->name.' - '.$user->name.' - Заказы';
$this->breadcrumbs = array(
    $user->name => array('user/profile', 'id'=>$user->id),
    'Заказы',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/index-order.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot.css.fontello').'.css'));
?>
<h1><?= $user->name; ?> - Заказы</h1>
<div class="panel">
    <?php $this->widget('bootstrap.widgets.TbGridView',array(
        'dataProvider'=>$model->search('user'),
        'filter'=>$model,
        'columns'=>array(
            array(
                'name'=>'id',
                'filterWrapTag'=>'a',
                'filterWrapHtmlOptions'=>array(
                    'rel'=>'tooltip',
                    'title'=>'Поиск по номеру заказа',
                ),
            ),
            array(
                'name'=>'name',
                'filterWrapTag'=>'a',
                'filterWrapHtmlOptions'=>array(
                    'rel'=>'tooltip',
                    'title'=>'Поиск по названию',
                ),
            ),
            array(
                'name'=>'cost',
                'filterWrapTag'=>'a',
                'filterWrapHtmlOptions'=>array(
                    'rel'=>'tooltip',
                    'title'=>'Поиск по стоимости',
                ),
                'value'=>'\'<span class="order-cost">\'.$data->cost.\'</span>\'',
                'type'=>'html',
                'header' => 'Стоимость, <i class="icon icon-rub"></i>'
            ),
            array(
                'name'=>'status',
                'filterWrapTag'=>'a',
                'filterWrapHtmlOptions'=>array(
                    'rel'=>'tooltip',
                    'title'=>'Поиск по статусу',
                ),
                'value' => '\'<span class="label label-\'.Order::getStatusClass($data->status).\'">\'.Order::getStatuses()[$data->status].\'</span>\'',
                'type' => 'html',
                'filter' => Order::getStatuses(),
            ),
            array(
                'name'=>'date',
                'filterWrapTag'=>'a',
                'filterWrapHtmlOptions'=>array(
                    'rel'=>'tooltip',
                    'title'=>'Поиск по дате',
                ),
                'value' => 'date("d.m.y H:i:s",strtotime($data->date))'
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'header'=>'Действия',
                'template' => '{view}'
            ),
        ),
        'template'=>'<div class="row-fluid"><div class="span6 cv">{pager}</div><div class="span6 cv">{summary}</div></div><div>{items}</div><div class="text-center">{pager}</div>',
        'type'=>'hover',
        'summaryText'=>'Страница {page} из {pages}<span class="hidden" id="totalCountProvider">{count}</span>',
    )); ?>
    <div>
        <strong>Итого</strong>: заказов - <span id="totalCount"></span>, на сумму - <a href="#" rel="tooltip" title="Без учёта стоимости изменённых товаров" id="totalCost"></a><i class="icon icon-rub"></i>.
    </div>
</div>
