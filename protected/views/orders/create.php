<?php
/**
 * @var OrdersController $this
 * @var Order $model
 */
$this->pageTitle = Yii::app()->name.' - Оформление заказа';
$this->breadcrumbs = array(
    'Оформление заказа',
);
Yii::app()->clientScript->registerScriptFile('http://api-maps.yandex.ru/2.1/?lang=ru_RU');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/ymap/client.js');
?>

<h1>Оформление заказа</h1>

<div class="alert alert-info">
    Поля помеченные "<span class="required">*</span>" обязательны для заполнения.
</div>
<div class="panel">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
    ));
    /** @var TbActiveForm $form */
    echo $form->errorSummary($model);
    if (!Yii::app()->user->isGuest):
    ?>
        <?= $form->textFieldRow($model,'name'); ?>
    <?php endif; ?>
    <?= $form->textFieldRow($model, 'telephone'); ?>
    <div class="control-group">
        <label class="control-label">Доставка <span class="required">*</span></label>
        <div class="controls">
            <div class="btn-group" data-toggle="buttons-radio">
                <button type="button" onclick="showMap();" class="btn active">Указать место доставки</button>
                <button type="button" onclick="hideMap();" class="btn">Заберу с завода</button>
            </div>
            <span class="help-inline"><?= CHtml::link('<i class="icon icon-info-sign"></i> О доставке',array('/shop/delivery'));?></span>
        </div>
    </div>
    <div class="control-group" id="map-row">
        <label class="control-label">Карта</label>
        <div class="controls">
            <div id="map-container" class="thumbnail">
                <div id="map"></div>
            </div>
        </div>
    </div>
    <?= $form->hiddenField($model, 'coord_x', array('id'=>'coordX')); ?>
    <?= $form->hiddenField($model, 'coord_y', array('id'=>'coordY')); ?>
    <?= $form->textAreaRow($model, 'comment'); ?>
    <div class="form-actions">
        <button class="btn btn-success">Оформить</button>
    </div>
    <?php $this->endWidget(); ?>
</div>