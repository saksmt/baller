<?php

class Cart
{
    /**
     * @var Cart singleton
     */
    private static $_me;

    private $_cart;

    public function __construct()
    {
        Yii::app()->session->open();
        if(!isset(Yii::app()->session['cart'])) {
            Yii::app()->session['cart'] = serialize(array());
        }
        $this->_cart = unserialize(Yii::app()->session['cart']);
    }

    private function _save()
    {
        Yii::app()->session['cart'] = serialize($this->_cart);
    }

    /**
     * @return Cart
     */
    public static function instance()
    {
        if(!isset(self::$_me)) {
            self::$_me = new Cart();
        }
        return self::$_me;
    }

    public function count()
    {
        return count($this->_cart);
    }

    public function add($product)
    {
        $this->_cart[] = $product;
        $this->_save();
    }

    public function getCart()
    {
        return $this->_cart;
    }

    public function clear()
    {
        $this->_cart = array();
        $this->_save();
    }

    public function __get($name)
    {
        if($name === 'cart') {
            return $this->getCart();
        }
    }

    public function remove($key)
    {
        if(isset($this->_cart[$key])) {
            unset($this->_cart[$key]);
            $this->_save();
            return true;
        }
        return false;
    }

    /**
     * @return Product[]
     */
    public function all()
    {
        return $this->_cart;
    }
} 