<?php

class RegisterForm extends UserDataModel {

    public $passwd_repeat;
    public $captcha;

    public function rules()
    {
        return array(
            array('captcha', 'required', 'on'=>'insert', 'enableClientValidation'=>false),
            array('email, name, passwd, passwd_repeat', 'required', 'enableClientValidation'=>true),
            array('login', 'length', 'max'=>50, 'min'=>3, 'enableClientValidation'=>true),
            array('email', 'length', 'max'=>100, 'enableClientValidation'=>true),
            array('email', 'email'),
            array('passwd', 'length', 'min'=>4),
            array('email, login', 'unique'),
            array('name', 'length', 'max'=>250, 'enableClientValidation'=>true),
            array('captcha', 'captcha', 'on'=>'insert', 'enableClientValidation'=>false),
            array('passwd_repeat', 'compare', 'compareAttribute'=>'passwd', 'enableClientValidation'=>true),
        );
    }

    final public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), array(
            'passwd_repeat'=>'Повторите пароль',
            'captcha'=>'Код с картинки',
        ));
    }

    public function beforeSave()
    {
        $this->encryptPassword();
        if(!isset($this->login)) {
            $this->login = $this->email;
        }
        $this->login = strtolower($this->login);
        $this->email = strtolower($this->email);
        return true;
    }

    private function encryptPassword()
    {
        $this->passwd = CPasswordHelper::hashPassword($this->passwd);
    }

    public function afterSave()
    {
        $this->sendMail();
    }

    private function sendMail()
    {
        $head = 'Content-Type: text/html; charset=UTF8\n'.
                'From: ООО БАЛЛЕР<robot@баллер.рф>';
        $body = '';
        return true;
    }
} 