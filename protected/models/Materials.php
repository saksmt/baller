<?php

class Materials extends CActiveRecord
{
    use ImageUploadTrait;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'tbl_materials';
    }

    public function rules() {
        return array(
            array('name, comment, code', 'required'),
            array('name', 'length', 'max'=>100),
            array('comment, code', 'length', 'max'=>255),
            array('image', 'default', 'setOnEmpty' => true, 'value' => null),
            array('image', 'safe'),
            array('id, name, comment, code, image', 'safe', 'on'=>'search'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'comment' => 'Комментарий',
            'code' => 'Код',
            'image' => 'Изображение',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('image', $this->image, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function relations()
    {
        return array(
            'products' => array(self::HAS_MANY, 'Product', 'material_id'),
        );
    }
}