<?php

/**
 * This is the model class for table "tbl_orders".
 *
 * The followings are the available columns in table 'tbl_orders':
 * @property string $id
 * @property string $name
 * @property string $user_id
 * @property string $telephone
 * @property string $date
 * @property integer $cost
 * @property double $coord_x
 * @property double $coord_y
 * @property string $comment
 * @property string $status
 */
class Order extends CActiveRecord
{
    const STATUS_PROCESSING = 'processing';
    const STATUS_DECLINED = 'declined';
    const STATUS_WAITING = 'waiting';
    const STATUS_GRABBED = 'grabbed';
    const STATUS_READY = 'ready';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telephone', 'required'),
			array('coord_x, coord_y', 'numerical','allowEmpty'=>true),
			array('name', 'length', 'max'=>100),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, user_id, telephone, date, cost, coord_x, coord_y, comment, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'rel' => array(self::HAS_MANY, 'OrderProduct', 'order_id'),
            'products' => array(self::HAS_MANY, 'Product', 'order_id', 'through'=>'rel'),
            'user'     => array(self::BELONGS_TO, 'UserDataModel', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Номер заказа',
			'name' => 'Название заказа',
			'telephone' => 'Контактный номер телефона',
			'date' => 'Дата заказа',
			'cost' => 'Стоимость',
			'comment' => 'Комментарий',
			'status' => 'Статус заказа',
		);
	}

    public static function getStatuses()
    {
        return array(
            self::STATUS_DECLINED => 'Отменён',
            self::STATUS_GRABBED => 'Отгружен',
            self::STATUS_PROCESSING => 'В обработке',
            self::STATUS_WAITING  => 'Ожидает обработку',
            self::STATUS_READY => 'Готов',
        );
    }

    public static function getStatusClass($status)
    {
        return array(
            self::STATUS_DECLINED => 'danger',
            self::STATUS_GRABBED => 'default',
            self::STATUS_PROCESSING => 'primary',
            self::STATUS_WAITING  => 'info',
            self::STATUS_READY => 'success',
        )[$status];
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical use-case:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($type = 'user')
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave()
    {
        if (!$this->isNewRecord) {
            return parent::beforeSave();
        }
        if (!Yii::app()->user->isGuest) {
            $this->user_id = Yii::app()->user->id;
        } else {
            $this->name = null;
        }
        $cost = 0;
        foreach ($this->products as $product) {
            $cost += $product->price;
        }
        $this->cost = $cost;
        $this->products = null;
        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
