<?php

/**
 * This is the model class for table "tbl_categories".
 *
 * The followings are the available columns in table 'tbl_categories':
 * @property string $id
 * @property string $name
 * @property string $parent
 */
class Categories extends CActiveRecord
{
    private $_path;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>100),
            array('parent_id', 'numerical', 'min'=>0, 'allowEmpty'=>true, 'integerOnly'=>true),
			array('parent_id', 'default', 'value'=>null),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'parent' => array(self::BELONGS_TO, 'Categories', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Categories', 'parent_id'),
            'products' => array(self::HAS_MANY, 'Product', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'parent_id' => 'Родительская категория',
            'childrenList' => 'Подкатегории'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function getAllFree($addEmpty = false)
    {
        $free =  self::model()->findAllBySql('SELECT * FROM `tbl_categories` AS `a` WHERE (SELECT COUNT(`id`) FROM `tbl_categories` WHERE `parent_id`=`a`.id) = 0');
        if($addEmpty) {
            $empty = new Categories();
            $empty->id = null;
            $empty->name = 'Не важно';
            $free[] = $empty;
        }
        return $free;
    }

    public function getChildrenList($path = null)
    {
        if ($path === null) {
            $path = $this->path;
        }
        $children = $this->children;
        $res = array();
        foreach ($children as $child) {
            $res[] = '<a href="'.Yii::app()->createUrl($path, array('id'=>$child->id)).'">'.$child->name.'</a>';
        }
        return implode(', ', $res);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->_path = $path;
    }
}
