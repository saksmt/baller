<?php

/**
 * LoginForm data keeper
 *
 * @author smt <kirillsaksin@yandex.ru>
 */
class LoginForm extends CFormModel
{
    public $login;
    public $password;
    public $rememberme=true;
    private $_identity;

    public function attributeLabels()
    {
        return array(
            'login'=>'Логин или Email',
            'password'=>'Пароль',
            'rememberme'=>'Запомнить меня',
        );
    }

    public function rules()
    {
        return array(
            array('login,password', 'required', 'enableClientValidation'=>'true'),
            array('rememberme', 'boolean'),
        );
    }

    public function login()
    {
        if($this->_identity===null) {
            $this->_identity = new UserIdentity($this->login,$this->password);
        }
        if($this->_identity->authenticate()) {
            $duration= ($this->rememberme) * 3600*24*365*2;
            Yii::app()->user->login($this->_identity,$duration);
            return true;
        }
        else {
            if($this->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID) {
                $this->addError('password', 'Неверный пароль!');
            }
            else {
                $this->addError('login', 'Пользователь с данным именем(или Email) не зарегестрирован');
            }
            return false;
        }
    }
}
