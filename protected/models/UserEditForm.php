<?php


class UserEditForm extends UserDataModel
{
    public $password;
    public $passwdRepeat;

    public function rules()
    {
        return array(
            array('login, email, name, password', 'required'),
            array('password', 'validatePassword'),
            array('login', 'length', 'max'=>50),
            array('email', 'length', 'max'=>100),
            array('name', 'length', 'max'=>250),
            array('role', 'length', 'max'=>8),
            array('passwd', 'length', 'min'=>4),
            array('passwdRepeat', 'compare', 'compareAttribute'=>'passwd', 'enableClientValidation'=>true),
        );
    }

    final public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), array(
            'passwd' => 'Новый пароль',
            'passwdRepeat'=>'Повторите пароль',
            'password' => 'Пароль',
        ));
    }

    public function validatePassword()
    {
        if (!CPasswordHelper::verifyPassword(
            $this->password,
            $this->model()->findByPk($this->id)->passwd
        )) {
            $this->addError('password', 'Не верный пароль!');
            return false;
        }
        return true;
    }

//    public function setAttributes($values, $safeOnly = true)
//    {
//        if(!is_array($values))
//            return;
//        $attributes = array_flip($safeOnly ? $this->getSafeAttributeNames() : $this->attributeNames());
//        foreach($values as $name => $value)
//        {
//            if ((!isset($value) || empty($value)) && in_array($name, array('passwd', 'password'))) {
//                continue;
//            }
//            if(isset($attributes[$name]))
//                $this->$name = $value;
//            elseif($safeOnly)
//                $this->onUnsafeAttribute($name,$value);
//        }
//    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $me = clone $this;
        $me->refresh();
        if (empty($this->passwd)) {
            $this->passwd = $me->passwd;
        } else {
            $this->passwd = CPasswordHelper::hashPassword($this->passwd);
        }
        return parent::beforeSave();
    }

} 