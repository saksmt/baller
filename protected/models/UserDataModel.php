<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users':
 * @property string $id
 * @property string $login
 * @property string $email
 * @property string $name
 * @property integer $passwd
 * @property string $role
 */
class UserDataModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, email, name, passwd', 'required'),
			array('login', 'length', 'max'=>50),
			array('email', 'length', 'max'=>100),
			array('name', 'length', 'max'=>250),
			array('role', 'length', 'max'=>8),
            array('login, email, name, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'orders' => array(self::HAS_MANY, 'Order', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'login' => 'Имя пользователя',
			'email' => 'Email',
			'name' => 'ФИО',
			'passwd' => 'Пароль',
            'role'=>'Статус',
		);
	}

    public static function roleToStatus($role)
    {
        return self::getStatuses()[$role];
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('login',$this->login,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getCode($action = '')
    {
        if(!isset($this->id)) {
            $this->refresh();
        }
        $hashFood = $this->attributes;
        sort($hashFood);
        return CPasswordHelper::hashPassword(serialize($hashFood).md5($action));
    }

    public function verifyCode($code, $action = '')
    {
        $hashFood = $this->attributes;
        sort($hashFood);
        return CPasswordHelper::verifyPassword(serialize($hashFood).md5($action), $code);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserDataModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function checkPassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->passwd);
    }

    public static function getStatuses()
    {
        return array('inactive'=>'Неактивный', 'user'=>'Активный', 'admin'=>'Администратор');
    }
}
