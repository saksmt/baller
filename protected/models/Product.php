<?php

/**
 * This is the model class for table "tbl_product".
 *
 * The followings are the available columns in table 'tbl_product':
 * @property string $id
 * @property string $title
 * @property string $width
 * @property string $height
 * @property string $length
 * @property string $category_id
 * @property string $material_id
 * @property string $description
 * @property string $status
 * @property string $images
 * @property float $price
 */
class Product extends CActiveRecord
{
    const AVAILABLE = 'available';
    const ORDER_ONLY = 'order_only';
    const NOT_AVAILABLE = 'not_available';


    public $widthTo;
    public $lengthTo;
    public $heightTo;
    public $priceTo;

    use ImageMultipleUploadTrait;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, width, height, length, category_id, material_id, description, status', 'required', 'on'=>array('insert','update')),
			array('title', 'length', 'max'=>50),
            array('width, height, length', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>9999),
            array('price', 'numerical'),
            array('category_id', 'exist', 'className'=>'Categories', 'attributeName'=>'id'),
			array('material_id', 'exist', 'className'=>'Materials', 'attributeName'=>'id'),
			array('status', 'in', 'range'=>array_flip($this->getStatuses())),
			array('images, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, width, height, length, category_id, material_id, description, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category'  => array(self::BELONGS_TO, 'Categories', 'category_id'),
            'material'  => array(self::BELONGS_TO, 'Materials', 'material_id'),
            'prototype' => array(self::HAS_ONE, 'Product', 'prototype_id'),
            'rel'       => array(self::HAS_MANY, 'OrderProduct','product_id'),
            'orders'    => array(self::MANY_MANY, 'Order', 'product_id','through'=>'rel'),
		);
	}

    public static function getStatuses($addEmpty = false)
    {
        $statuses = array(
            self::AVAILABLE=>'В наличии',
            self::ORDER_ONLY=>'Только на заказ',
            self::NOT_AVAILABLE=>'Нет в наличии',
        );
        if($addEmpty) {
            $statuses[null] = 'Не важно';
        }
        return $statuses;
    }

    public function getUserStatus()
    {
        return self::getStatuses()[$this->status];
    }

    public function isAvailable()
    {
        if(Yii::app()->user->checkAccess('order_only')) {
            return $this->status !== self::NOT_AVAILABLE;
        } else {
            return $this->status === self::AVAILABLE;
        }
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'width' => 'Ширина',
			'height' => 'Высота',
			'length' => 'Глубина',
			'category_id' => 'Категория',
			'material_id' => 'Материал',
			'description' => 'Описание',
			'status' => 'Наличие',
			'images' => 'Изображения',
            'price' => 'Цена',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        if (isset($this->widthTo) || isset($this->width)) {
            $criteria->addBetweenCondition('width', $this->width, $this->widthTo);
        }
        if (isset($this->height) || isset($this->heightTo)) {
            $criteria->addBetweenCondition('height', $this->width, $this->widthTo);
        }
        if (isset($this->length) || isset($this->lengthTo)) {
            $criteria->addBetweenCondition('length', $this->length, $this->lengthTo);
        }
        if (isset($this->price) || isset($this->priceTo)) {
            $criteria->addBetweenCondition('price', $this->price, $this->priceTo);
        }
		$criteria->compare('title',$this->title,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('material_id',$this->material_id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status,true);
        $criteria->compare('custom',false);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getStatusAsLabelClass()
    {
        return array(
            self::AVAILABLE => 'success',
            self::ORDER_ONLY => 'info',
            self::NOT_AVAILABLE => 'warning',
        )[$this->status];
    }

    public static function random()
    {
        return self::model()->find('1=1 ORDER BY RAND()');
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getStatusSort()
    {
        return 'CASE '.
            'WHEN status="not_available" THEN 0 '.
            'WHEN status="order_only" THEN 1 '.
            'ELSE 2 '.
            'END ';
    }
}
