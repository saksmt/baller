<?php

/**
 * Redefinition of CWebUser for RBAC
 *
 * @author smt <kirillsaksin@yandex.ru>
 */
class WebUser extends CWebUser
{
    private $_model = null;
    public $loginUrl = array('/user/login');
 
    function getRole()
    {
        if($user = $this->getModel()){
            return $user->role;
        }
        return 'guest';
    }
 
    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null){
            $this->_model = UserDataModel::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }

    public function __get($name)
    {
        if($name == 'isGuest') {
            return parent::__get($name);
        }
        if (substr($name,0,2) == 'is') {
            return ('is'.ucfirst($this->getRole()) == $name);
        } else {
            return parent::__get($name);
        }
    }
}
