<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $readableName = null;

    public function getMenu()
    {
        if(!isset($this->readableName)) {
            return array();
        }
        $menu = array(
            array('label'=>'Список '.$this->readableName['many'], 'url'=>array($this->uniqueId.'/index')),
            array('label'=>'Администрирование '.$this->readableName['many'], 'url'=>array($this->uniqueId.'/admin')),
            array('label'=>'Создание '.$this->readableName['one'], 'url'=>array($this->uniqueId.'/create')),
        );
        $id = Yii::app()->request->getParam('id');
        if(isset($id)) {
            $menu[] = array('label'=>'Просмотр '.$this->readableName['one'], 'url'=>array($this->uniqueId.'/view','id'=>$id));
            $menu[] = array('label'=>'Изменение '.$this->readableName['one'], 'url'=>array($this->uniqueId.'/update','id'=>$id));
        }
        $checkedMenu = array();
        foreach($menu as $action) {
            if(Yii::app()->user->checkAccess(str_replace('/','.',$action['url'][0]))) {
                $checkedMenu[] = $action;
            }
        }
        if(count($checkedMenu) < 2) {
            return array();
        }
        return $checkedMenu;
    }

    public function getRightMenu()
    {
        return array();
    }

    public function redirectNotAuthorized($rule) {
        $user = Yii::app()->user;
        if($user->isGuest) {
            $this->redirect(Yii::app()->createUrl('user/login'));
        }
        else if($this->action->id == 'login' || $this->action->id == 'register'){
            $this->redirect(Yii::app()->createUrl('user/profile'));
        }
        else {
            throw new CHttpException(403);
        }
        Yii::app()->end();
    }

    protected function performAjaxValidation($model)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model->scenario = 'ajax';
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}