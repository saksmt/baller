<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    protected $_id;
    public function authenticate()
    {
        $user = UserDataModel::model()->find('LOWER(login)=:username OR LOWER(email)=:username', array('username'=>strtolower($this->username)));
        if($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        elseif(!$user->checkPassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else {
            $this->errorCode = self::ERROR_NONE;
            $this->_id = $user->id;
            $this->setUserData($user);
        }
        return !$this->errorCode;
    }

    private function setUserData($model)
    {
        $this->setState('name', $model->name);
        $this->setState('login', $model->login);
        $this->setState('email', $model->email);
    }

    public function getId()
    {
        return $this->_id;
    }
}