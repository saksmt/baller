<?php

/**
 * Provides file save functions
 * Needs table column with type @a VARCHAR(85)
 * You can write "use ImageUploadTrait" to use default params with @a savePathPostfix equal to your class name
 * @brief Behavior to manipulate images
 */
class ImageUploadBehavior extends CActiveRecordBehavior {
    /**
     * @var string Column name in database
     */
    public $attributeName = 'image';
    /**
     * @var string Alias to folder where we would store images
     */
    public $savePathAlias = 'webroot.images';
    /**
     * @var string Alias postfix
     */
    public $savePathPostfix = null;
    /**
     * @var array Scenarios to validate
     */
    public $scenarios = array('insert', 'update');
    /**
     * @var string|array File types
     */
    public $fileTypes = 'png,jpg,tiff,svg';
    /**
     * @var bool Is it multiple file upload?
     */
    public $multiple = false;

    public function getSavePath() {
        return Yii::getPathOfAlias($this->savePathAlias.(isset($this->savePathPostfix)?'.'.$this->savePathPostfix:'')).DIRECTORY_SEPARATOR;
    }

    public function attach($owner) {
        parent::attach($owner);
        if(in_array($owner->scenario,$this->scenarios)) {
            $fileValidator = CValidator::createValidator('file',$owner,$this->attributeName,
                array('types'=>$this->fileTypes,'allowEmpty'=>'true'));
            $owner->validatorList->add($fileValidator);
        }
    }

    public function beforeSave($event) {
        if(!$this->multiple) {
            if (in_array($this->owner->scenario, $this->scenarios) && ($file = CUploadedFile::getInstance($this->owner, $this->attributeName))) {
                $this->deleteFile();
                $filename = sha1($file->name) . sha1($file->size . $file->type) . '.' . $file->extensionName;
                $this->owner->setAttribute($this->attributeName, $filename);
                return boolval($file->saveAs($this->savePath . $filename));
            }
        } else {
            if(
                in_array($this->owner->scenario, $this->scenarios) &&
                (!empty($files = CUploadedFile::getInstances($this->owner, $this->attributeName)))
            ) {
                $this->deleteFile();
                $result = true;
                $filenames = array();
                foreach ($files as $file) {
                    $filename = sha1($file->name) . sha1($file->size . $file->type) . '.' . $file->extensionName;
                    $filenames[] = $filename;
                    $result &= boolval($file->saveAs($this->savePath . $filename));
                }
                $this->owner->setAttribute($this->attributeName, serialize($filenames));
                return $result;
            }
        }
        return true;
    }

    public function beforeDelete($event) {
        $this->deleteFile();
    }

    private function deleteFile() {
        if(!$this->multiple) {
            $filePath = $this->savePath . $this->owner->getAttribute($this->attributeName);
            if (@is_file($filePath)) {
                @unlink($filePath);
            }
        } else {
            $filenames = unserialize($this->owner->getAttribute($this->attributeName));
            if(!is_array($filenames) || empty($filenames)) {
                return;
            }
            foreach($filenames as $filename) {
                $filePath = $this->savePath . $filename;
                if(@is_file($filePath)) {
                    @unlink($filePath);
                }
            }
        }
    }
}

Yii::import('application.components.ImageUploadTrait');