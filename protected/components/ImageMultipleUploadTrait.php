<?php

Yii::import('application.components.ImageUploadBehavior');

trait ImageMultipleUploadTrait {
    public function behaviors() {
        return array(
            'imageUpload' => array(
                'class' => 'application.components.ImageUploadBehavior',
                'savePathPostfix' => __CLASS__,
                'attributeName' => 'images',
                'multiple' => true,
            ),
        );
    }
}