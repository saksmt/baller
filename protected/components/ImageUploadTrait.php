<?php

Yii::import('application.components.ImageUploadBehavior');

trait ImageUploadTrait {
    public function behaviors() {
        return array(
            'imageUpload' => array(
                'class' => 'application.components.ImageUploadBehavior',
                'savePathPostfix' => __CLASS__,
            ),
        );
    }
}