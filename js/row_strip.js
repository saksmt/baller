/**
 * @author smt <kirillsaksin@yandex.ru>
 */

var stripped_text = [];
var stripped_selected = null;

function stripRow(selector) {
    var stID = ((selector.attr('data-st-id') === undefined) ? stripped_text.length : selector.attr('data-st-id'));
    selector.attr('data-st-id', stID);
    if(stripped_text[stID] === undefined) {
        stripped_text[stID] = $(selector).text();
    }
    var strip_size = selector.width()/12;
    console.log(strip_size);
    if(stripped_text[stID].length > strip_size)
        $(selector).text(stripped_text[stID].substr(0,strip_size)+'...');
};

function unstripRow(selector) {
    $(selector).text(stripped_text[selector.attr('data-st-id')]);
};

function stripSelectionChanged(id) {
    if(stripped_selected !== null)
        stripRow(stripped_selected);
    stripped_selected = $(".items").find(".selected").find(".news-contents");
    unstripRow(stripped_selected);
};