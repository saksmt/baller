$(function(){
    $('.hide-menu').click(function(){
        $('#submenu').slideUp(function(){$('.show-menu').fadeIn()});
        return false;
    });
    $('.show-menu').click(function(){
        $(this).fadeOut(function(){$('#submenu').slideDown()});
        return false;
    });
    $(window).scroll(function(){
        if(window.scrollY > window.scrollMaxY/10) {
            $('#gotopButton').fadeIn();
        } else {
            $('#gotopButton').fadeOut();
        }
    });
    $('#gotopButton').click(function(){
        $(document.documentElement).animate({scrollTop: 0});
        return false;
    }).hide();
});