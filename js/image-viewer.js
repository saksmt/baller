/**
 * Class for image viewing
 * @param modalId string Selector for modal which would be used as "fullscreen"
 * @param imageContainerId string Selector for base image container
 * @constructor
 * @author smt <kirillsaksin@yandex.ru>
 */
function ImageViewer(modalId, imageContainerId) {
    this.modal = $(modalId);
    this.container = $(imageContainerId);
    this.container.find('.image-main').first().click(this,this.showFullScreen);
    this.container.find('.image-mini').click(this,this.setAsMain);
    $('#viewer-main').click(this,this.next).height(295);
    this.changing = false;
}
ImageViewer.prototype.setAsMain = function(event) {
    event.preventDefault();
    if(event.data.changing) {
        return false;
    }
    event.data.changing = true;
    var $this = $(this).find('div').first();
    var newImage = $this.css('background-image');
    var images;
    var mainImage;
    if (!$this.parent().hasClass('modal-image')) {
        images = event.data.container.find('.image-mini');
        mainImage = event.data.container.find('.image-main').find('div').first();
        mainImage.fadeOut(300, function () {
            mainImage.css('background-image', newImage);
            images.filter('.active').removeClass('active');
            $this.parent().addClass('active');
            mainImage.fadeIn(300);
        });
    } else {
        images = $('#viewer-menu').find('.image-mini');
        mainImage = $('#viewer-main').find('div').first();
        mainImage.fadeOut(300, function () {
            mainImage.css('background-image', newImage);
            images.filter('.active').removeClass('active');
            $this.parent().addClass('active');
            mainImage.fadeIn(300);
        });
    }
    setTimeout(function(){event.data.changing = false;}, 300);
    return false;
};
ImageViewer.prototype.showFullScreen = function(event) {
    event.preventDefault();
    var $mainImage = $('#viewer-main');
    var $menu = $('#viewer-menu');
    $mainImage.find('div').first().css('background-image',event.data.container.find('.image-main').find('div').first().css('background-image'));
    $menu.html(event.data.container.find('.panel-mini').first().html());
    $menu.find('.image-mini').addClass('modal-image');
    $menu.find('.image-mini').click(this,event.data.setAsMain);
    event.data.modal.modal('show');
    return false;
};
ImageViewer.prototype.next = function(event) {
    event.preventDefault();
    if(event.data.changing) {
        return false;
    }
    event.data.changing = true;
    var $mainImage = $(this).find('div').first();
    var images = $('#viewer-menu');
    var $current = images.find('.active').first();
    var $new = $current.next();
    if($new === undefined || $new.length === 0) {
        $new = images.find('.image-mini').first();
    }
    $mainImage.fadeOut(300,function(){
        $mainImage.css('background-image',$new.find('div').first().css('background-image'));
        $current.removeClass('active');
        $new.addClass('active');
        $mainImage.fadeIn(300);
    });
    setTimeout(function(){event.data.changing = false;}, 300);
    return false;
};