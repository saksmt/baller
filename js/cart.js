function deleteFromCart(event) {
    event.preventDefault();
    var $this = $(this);
    var cartBadge = $('#cart-link-badge');
    var remain = parseInt(cartBadge.text())-1;
    if(!remain) {
        $('#clearCart').click();
        return false;
    }
    $.post(window.address,{'id':$this.attr('data-key')},function(response){
        if(response) {
            cartBadge.html(parseInt(cartBadge.text())-1);
            $this.parent().parent().remove();
        } else {
            $('#alerts').append(window.errorMessage);
        }
    },'json');
    return false;
}
$(function(){
    $('.delete-cart-element').click(deleteFromCart);
});