function showTotal () {
    var total = 0;
    $('.order-cost').each(function () {
        total += parseFloat($(this).text());
    });
    $('#totalCost').html(total);
    var data = $('#totalCountProvider').text();
    if (data === '' || data === undefined) {
        data = '0';
    }
    $('#totalCount').html(data);
}
$(showTotal);