var searchState = false;
function registerForms(id,data) {
    if(searchState) {
        $('.search-form').toggle();
        $('.search-button').toggleClass('active');
    }
    $('.search-button').click(function(e){
        $('.search-form').toggle();
        $(this).toggleClass('active');
        searchState = !searchState;
        e.preventDefault();
        return false;
    });
    $('.search-form form').submit(function(e){
        e.preventDefault();
        $.fn.yiiListView.update(id, {
            data: $(this).serialize()
        });
        return false;
    });
    $('.short-search-form').submit(function(e){
        e.preventDefault();
        $.fn.yiiListView.update(id, {
            data: $(this).serialize()
        });
        return false;
    });
}