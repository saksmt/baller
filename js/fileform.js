function fileLoaded() {
    var $this = $(this);
    $this.next().text('');
    $this.next().html('<strong>Выбран</strong>: '+$this.val());
};

$(function() {
    var file = $('input[type="file"]');
    file.wrap('<div class="row-fluid" />');
    file.hide();
    file.after('<span class="span10 align-to-btn"></span>');
    file.before('<button type="button" class="btn btn-primary span2">Выбрать</button>');
    file.prev().click(function() {
        file.click();
    });
    file.change(fileLoaded);
});
