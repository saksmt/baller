ymaps.ready(init);

function init() {
    if (window.position === undefined) {
        return;
    } else {
        $('#map').html('');
    }
    var myMap = new ymaps.Map('map', {
        center: [55.753994, 37.622093], // Saint-Petersburg
        zoom: 9
    });
    //              Coords x, y
    ymaps.geocode(window.position, {
        results: 1
    }).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0),
        coords = firstGeoObject.geometry.getCoordinates(),
        bounds = firstGeoObject.properties.get('boundedBy');
        console.log(firstGeoObject);
        firstGeoObject.properties.set('iconContent', 'Доставка');
        firstGeoObject.options.set('preset', 'islands#blueStretchyIcon');
        myMap.geoObjects.add(firstGeoObject);
        myMap.setBounds(bounds, {
            checkZoomRange: true
        });
    });
}