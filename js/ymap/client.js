ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [59.939095, 30.315868], // Saint-Petersburg
        zoom: 12,
        controls: []
    }),
    mySearchControl = new ymaps.control.SearchControl({
        options: {
            noPlacemark: true
        }
    }),
    mySearchResults = new ymaps.GeoObjectCollection(null, {
        hintContentLayout: ymaps.templateLayoutFactory.createClass('$[properties.name]')
    });
    myMap.controls.add(mySearchControl);
    myMap.geoObjects.add(mySearchResults);
    mySearchResults.events.add('click', function (e) {
        e.get('target').options.set('preset', 'islands#redIcon');
    });
    /// Add result to map
    mySearchControl.events.add('resultselect', function (e) {
        var index = e.get('index');
        mySearchControl.getResult(index).then(function (res) {
            mySearchResults.add(res);
            /// Saving coordinates for form
            var coordinates = res.geometry.getCoordinates();
            $('#coordX').val(coordinates[0]);
            $('#coordY').val(coordinates[1]);
        });
    }).add('submit', function () {
        /// Cleared map? Clearing input!
        $('#coordX').val('');
        $('#coordY').val('');
        mySearchResults.removeAll();
    })
});

function showMap() {
    $('#map-row').slideDown();
};
function hideMap() {
    $('#map-row').slideUp();
    $('#coordX').val('');
    $('#coordY').val('');
}