var sizes = true;
function showSizes() {
    sizes = false;
    $('#sizes').slideDown();
}
function hideSizes() {
    sizes = true;
    $('#sizes').slideUp();
}
$(function(){
    $('#submit').click(function(){
        if (sizes) {
            $('#sizes').find('input').val('0');
        }
        $(this).prev().click();
    });
    hideSizes();
});