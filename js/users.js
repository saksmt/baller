function showUserData(user) {
    $('#userName').text(user.name);
    $('#userDataModalSubmit').hide();
    $('#userDataModal > .modal-body').html(
        '<p><strong>ФИО: </strong>'+user.name+'</p>'+
        '<p><strong>Имя пользователя: </strong>'+user.login+'</p>'+
        '<p><strong>Email: </strong>'+user.email+'</p>'+
        '<p><strong>Статус: </strong>'+user.status+'</p>'
    );
    $('#userDataModal').modal('show');
}

function showChangePromt(user) {
    $('#userName').text(user.name);
    $('#userDataModalSubmit').show();
    $('#userDataModal > .modal-body').html(
        '<form id="changeUser" data-id="'+user.id+'">'+
        '<p><strong>ФИО: </strong>'+user.name+'</p>'+
        '<p><strong>Имя пользователя: </strong>'+user.login+'</p>'+
        '<p><strong>Email: </strong>'+user.email+'</p>'+
        '<p><strong>Статус: </strong>'+statuses+'</p>'+
        '</form>'
    );
    $('#userDataModal').modal('show');
}

function submitChanges(route) {
    $('#userLoading').show();
    $.post(route+'/'+$('#changeUser').attr('data-id'),$('#changeUser').serialize(), function(response){
        console.log(response);
        if(response.code !== 200) {
            return;
        }
        $.fn.yiiGridView.update('categories-grid');
        $('#userDataModal').modal('hide');
        $('#userLoading').hide();
    }, 'json');
    return false;
}