function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
};

Date.fromDateTime = function(t) {
    t = t.split(/[- :]/);
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    return d;
};

Date.prototype.toDateTime = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()+1) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

$(function(){
    $('.date > input').datepicker()
        .on('changeDate', function(e) {
            $('.realInput').val(e.date.toDateTime());
        })
        .datepicker('setValue',Date.fromDateTime($('.realInput').val()));
    $('#dateLabel').click(function(){
        $('.date > input').focus();
    });
    $('.date > span').click(function(){
        $('.date > input').focus();
    });
});
