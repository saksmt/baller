$(function(){
    $('#changeOrderStatusButton').click(function (e) {
        var $this = $(this);
        if ($('#orderChangeSelect').length === 0) {
            $this.parent().before('<select id="orderChangeSelect">' +
            '<option value="processing">В обработке</option>' +
            '<option value="declined">Отменён</option>' +
            '<option value="grabbed">Отгружен</option>' +
            '<option value="ready">Готов</option>' +
            '<option value="waiting">Ожидет обработку</option>' +
            '</select>');
            $('#orderChangeSelect').change(function () {
                $.post(window.adminOrderUrl, {'newStatus':$('#orderChangeSelect').val()}, function (response) {
                    if (response.status === 200) {
                        $this.parent().find('#status').html(response.newStatus);
                        $this.parent().removeClass(response.oldClass);
                        $this.parent().addClass(response.newClass);
                        $this.parent().show();
                        $('#orderChangeSelect').hide();
                        $('#orderChangeSelect').val('');
                    } else {
                        $('#alerts').append(response.message);
                    }
                });
            });
        }
        $this.parent().hide();
        $('#orderChangeSelect').show();
        e.preventDefault();
        return false;
    });
});