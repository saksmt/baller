<?php /* @var $this Controller */?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
    <?php echo CHtml::cssFile(Yii::app()->request->baseUrl.'/css/corrections.css');?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/menu.js');?>
</head>

<body>

<?php
if(!empty($this->menu)) {
    $rightMenu = $this->rightMenu;
    $rightMenu[] = array(
        'label' => 'Наверх',
        'icon'=>'arrow-up',
        'url'=>'#',
        'id'=>'gotopButton',
        'linkOptions'=>array('id'=>'gotopButton'),
    );
    echo CHtml::link('<i class="icon icon-chevron-down"></i>', '#', array(
        'class'=>'show-menu',
        'rel'=>'tooltip',
        'title'=>'Показать меню',
        'data-placement'=>'bottom',
    ));
    $this->widget('bootstrap.widgets.TbNavbar',array(
        'items'=>array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>$this->menu,
            ),
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>$rightMenu,
                'encodeLabel'=>false,
                'htmlOptions'=>array('class'=>'pull-right'),
            ),
            '<a class="hide-menu" href="#" rel="tooltip" title="Скрыть меню" data-placement="bottom"><i class="icon icon-chevron-up"></i></a>'
        ),
        'brand'=>'',
        'htmlOptions'=>array(
            'id'=>'submenu',
        ),
    ));
}
?>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'type'=>'inverse',
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Главная', 'url'=>array('/site/index')),
                array('label'=>'О нас', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Новости компании', 'url'=>array('news/index'), 'items'=>(
                    Yii::app()->user->isAdmin?array(
                        array('label'=>'Просмотр', 'url'=>array('/news/index')),
                        array('label'=>'Управление', 'url'=>array('/news/admin')),
                    ):null
                )),
                array('label'=>'Контакты', 'url'=>array('/site/page', 'view'=>'contact')),
                array('label'=>'Магазин', 'url'=>array('/shop/index')),
            ),
        ),
        array(
            'class'=>'bootsrap.widgets.TbMenu',
            'htmlOptions'=>array('class'=>'pull-right'),
            'items'=>array(
                '---',
                array('label'=>'Вход', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Регистрация', 'url'=>array('/user/register'), 'visible'=>Yii::app()->user->isGuest),
                array(
                    'label'=>'Профиль ('.Yii::app()->user->name.')',
                    'url'=>array('/user/profile'),
                    'visible'=>!Yii::app()->user->isGuest,
                    'items'=>Yii::app()->user->isAdmin?array(
                        array('label'=>'Моя страница','url'=>array('/profile')),
                        array('label'=>'Административная панель','url'=>array('/panel')),
                    ):null,
                ),
                array('label'=>'Выход', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest),
            ),
        ),
    ),
)); ?>

<div class="container-fluid" id="page<?= !empty($this->menu)?>">
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true,
        'fade'=>true,
        'htmlOptions'=>array('id'=>'alerts'),
    ));?>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
            'homeLink'=>'<a href="'.Yii::app()->baseUrl.'">Главная</a>',
		)); ?><!-- breadcrumbs -->
	<?php endif?>

    <div class="row-fluid">
        <?php echo $content; ?>
    </div>

	<div class="clear"></div>

</div><!-- page -->

<div class="text-center">
        ООО БАЛЛЕР 2014.<br>
        Разработка <a href="http://vk.com/saksmt">smt</a> 2014.
</div><!-- footer -->

</body>
</html>
