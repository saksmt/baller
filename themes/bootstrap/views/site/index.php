<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php
$this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
    'heading'=>'Добро пожаловать.',
));
?>

<p>Добро пожаловать на сайт мебельного магазина "ООО Баллер".</p>

<?php $this->endWidget(); ?>

<?php $this->widget('ext.local.widgets.NewsWidget', array('params'=>array(
    'model'=>new Gallery(),
    'struct'=>array('label'=>'title', 'image'=>'image'),
    'limit'=>10,
    'order'=>'RAND()',
)));?>
